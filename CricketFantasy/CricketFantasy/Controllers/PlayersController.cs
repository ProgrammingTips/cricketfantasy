﻿using CricketFantasy.DataLayer;
using CricketFantasy.DataLayer.Model;
using CricketFantasy.DataTransferObjects.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CricketFantasy.Controllers
{
    public class PlayersController : Controller
    {
        // GET: Players
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult CreatePlayer(int PlayerId, int TeamId)
        {
            PlayersDto model = new PlayersDto();

            if (PlayerId != 0)       
            {
                var TeamInfo = DataAccess.Instance.PlayersActions.Get(PlayerId);
                model.TeamId = TeamId;
                model.PlayerName = TeamInfo.PlayerName;
             //   model.isCaptain = TeamInfo.IsCaptain;
                model.PlayerType = TeamInfo.PlayerType;

            }
            return PartialView(model);
        }


        [HttpPost]
        public ActionResult CreatePlayer(PlayersDto model)
        {

            if (model.PlayerId != 0)
            {
                var PlayerInfo = DataAccess.Instance.PlayersActions.Get(model.PlayerId);
                PlayerInfo.PlayerName = model.PlayerName;
              //  PlayerInfo.IsCaptain = model.isCaptain;
                PlayerInfo.PlayerType = model.PlayerType;
                DataAccess.Instance.PlayersActions.Change(PlayerInfo);
            }
            else
            {
                Player player = new Player();
//                player.team_Id = model.TeamId;
                player.PlayerName = model.PlayerName;
    //            player.IsCaptain = model.isCaptain;
  //            //  player.SystemUserId = "0";
                player.IsActive = true;
                player.PlayerType = model.PlayerType;
                DataAccess.Instance.PlayersActions.Add(player);
            }

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult GetPlayersData(int TeamId)
        {
           // int[] PlayersAssigned = DataAccess.Instance.TeamPlayerAction.GetAll().Where(x => x.TeamId != TeamId).Select(x => x.PlayerId).ToArray();
            List<PlayersDto> Players = DataAccess.Instance.PlayersActions.GetAll().AsEnumerable().Select(x => new PlayersDto {
                PlayerId = x.Id,
                PlayerName = x.PlayerName,
                PlayerType = x.PlayerType,
                IsInCurrentTeam = DataAccess.Instance.TeamPlayerAction.GetAll().Where(y=>y.TeamId == TeamId && y.PlayerId == x.Id).Any()
            }).ToList();
            return PartialView(Players);
        }
    }
}