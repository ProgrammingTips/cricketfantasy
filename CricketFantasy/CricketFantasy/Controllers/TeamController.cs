﻿using CricketFantasy.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CricketFantasy.DataTransferObjects.Models;
using CricketFantasy.DataLayer.Model;

namespace CricketFantasy.Controllers
{
    public class TeamController : Controller
    {
        // GET: Team
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult TeamManagement()
        {
            return View();
        }

         public ActionResult VirtualTeamManagement()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ExistingTeam(string TeamName, int TeamId)
        {
            if(TeamId == 0)
            {
                return Json(!DataAccess.Instance.TeamAction.GetAll().Where(x => x.TeamName == TeamName).Any(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(!DataAccess.Instance.TeamAction.GetAll().Where(x => x.TeamName == TeamName && x.Id != TeamId).Any(), JsonRequestBehavior.AllowGet);
            }

        }


        public PartialViewResult GetTeamData()
        {
            var d = DataAccess.Instance.TeamAction.GetAll().Where(x=>x.IsActive != false && x.IsVirtualTeam != true).AsEnumerable().Select(x => new TeamDTO
            {
                TeamId = x.Id,
                TeamName = x.TeamName,
                Country = x.country,
                PlayersCount = DataAccess.Instance.TeamPlayerAction.GetAll().Where(y => y.TeamId == x.Id).Count(),
            }).ToList();
            return PartialView(d);
        }
        
        public PartialViewResult GetVirtualTeamData()
        {
            var d = DataAccess.Instance.TeamAction.GetAll().Where(x=>x.IsActive == true && x.IsVirtualTeam == true).AsEnumerable().Select(x => new TeamDTO
            {
                TeamId = x.Id,
                TeamName = x.TeamName,
                Country = x.country,
                PlayersCount = DataAccess.Instance.TeamPlayerAction.GetAll().Where(y => y.TeamId == x.Id).Count(),
            }).ToList();
            return PartialView(d);
        }

        public PartialViewResult CreateTeam(int TeamId)
        {
            CreateTeamDto model = new CreateTeamDto();
            
            if (TeamId != 0)
            {
                var TeamInfo = DataAccess.Instance.TeamAction.Get(TeamId);
                model.TeamId = (int)TeamId;
                model.TeamName = TeamInfo.TeamName;
                model.CountryName = TeamInfo.country;
            }
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult CreateTeam(CreateTeamDto TeamData)
        {
            CreateTeamDto model = new CreateTeamDto();

            if (TeamData.TeamId != 0)
            {
                Team teaminfo = DataAccess.Instance.TeamAction.Get(TeamData.TeamId);
                teaminfo.TeamName = TeamData.TeamName;
                teaminfo.country = TeamData.CountryName;
                DataAccess.Instance.TeamAction.Change(teaminfo);
            }
            else
            {
                Team teaminfo = new Team();
                teaminfo.TeamName = TeamData.TeamName;
                teaminfo.country = TeamData.CountryName;
                teaminfo.IsActive = true;
                teaminfo.CreateUser = "0";
                teaminfo.CreateDate = DateTime.Now;
                DataAccess.Instance.TeamAction.Add(teaminfo);
                TeamData.TeamId = teaminfo.Id;
            }
            return Json(new { TeamId = TeamData.TeamId }, JsonRequestBehavior.AllowGet);
        }

    }
}