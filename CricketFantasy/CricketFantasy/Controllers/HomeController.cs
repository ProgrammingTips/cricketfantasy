﻿using CricketFantasy.DataLayer;
using CricketFantasy.DataLayer.Model;
using CricketFantasy.DataTransferObjects.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace CricketFantasy.Controllers
{
    [Authorize]
    [Authorize(Roles = "Admin")]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            CountersDTO model = new CountersDTO();
            model.TotalPlayers = DataAccess.Instance.PlayersActions.GetAll().Count();
            model.TotalUsers = DataAccess.Instance.UsersAction.GetAll().Count();
            model.TotalTeams = DataAccess.Instance.TeamAction.GetAll().Where(x=>x.IsVirtualTeam != true).Count();
            model.TotalVirtualTeams = DataAccess.Instance.TeamAction.GetAll().Where(x => x.IsVirtualTeam == true).Count();
            model.TotalTournaments = DataAccess.Instance.TournamentAction.GetAll().Count();
            return View(model);
        }

        public ActionResult UserManagement()
        {
            return View();
        }


        public PartialViewResult GetUsers()
        {
            var d = DataAccess.Instance.UsersAction.GetAll().AsEnumerable().Select(x => new UsersDTO {
                UserName = x.UserName,
                Email = x.Email,
                UserId = x.Id,
                RoleTitle = GetUseRole(x.Id),
            }).ToList();
            return PartialView(d);
        }
        [HttpPost]
        public PartialViewResult GetUsers(string p_strName)
        {
            var d = DataAccess.Instance.UsersAction.GetAll().AsEnumerable().Select(x => new UsersDTO {
                UserName = x.UserName,
                Email = x.Email,
                UserId = x.Id,
                RoleTitle = GetUseRoleByName(p_strName),
            }).Where(x=>x.RoleTitle == p_strName).ToList();
            return PartialView(d);
        }

        private string GetUseRoleByName(string p_strName)
        {
            string Role = String.Empty;
            var RoleId = DataAccess.Instance.RoleAction.GetAll().Where(x => x.Name == p_strName).Select(y=> new { Id = y.Id }).FirstOrDefault();
            var UserRoleInfo = DataAccess.Instance.UserRoleAction.GetAll().Where(x => x.RoleId==RoleId.Id).FirstOrDefault();
            if (UserRoleInfo != null)
            {
                Role = UserRoleInfo.AspNetRole.Name;
            }

            return Role;
        }
        
        private string GetUseRole(string id)
        {
            string Role = String.Empty;
            var UserRoleInfo = DataAccess.Instance.UserRoleAction.GetAll().Where(x => x.UserId == id).FirstOrDefault();
            if (UserRoleInfo != null)
            {
                Role = UserRoleInfo.AspNetRole.Name;
            }

            return Role;
        }

        public PartialViewResult CreateUser(string UserId)
        {
            CreateUserDTO model = new CreateUserDTO();
            model.RolesList = DataAccess.Instance.RoleAction.GetAll().Select(x => new RolesDTO
            {
                RoleId = x.Id,
                RoleName = x.Name
            }).ToList();
            if (UserId != "0")
            {
                var UserInfo = DataAccess.Instance.UsersAction.Get(UserId);
                model.UserId = UserId;
                // model.Password = Decrypt(UserInfo.PasswordHash);
                model.Password = "";
                model.Email = UserInfo.Email;
                model.UserName = UserInfo.UserName;
                model.UserRoleId = UserInfo.AspNetUserRoles.FirstOrDefault() !=  null ? UserInfo.AspNetUserRoles.FirstOrDefault().RoleId : string.Empty;
            }
            return PartialView(model);
        }

        public string Decrypt(string cipher)
        {
            if (cipher == null) throw new ArgumentNullException("cipher");

            //parse base64 string
            byte[] data = Convert.FromBase64String(cipher);

            //decrypt data
            byte[] decrypted = ProtectedData.Unprotect(data, null, DataProtectionScope.CurrentUser);
            return Encoding.Unicode.GetString(decrypted);
        }

        //public static string DecryptText(string encryptedText)
        //{
        //    RC2CryptoServiceProvider rc2CSP = new RC2CryptoServiceProvider();
        //    ICryptoTransform decryptor = rc2CSP.CreateDecryptor(Convert.FromBase64String(c_key), Convert.FromBase64String(c_iv));
        //    using (MemoryStream msDecrypt = new MemoryStream(Convert.FromBase64String(encryptedText)))
        //    {
        //        using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
        //        {
        //            List<Byte> bytes = new List<byte>();
        //            int b;
        //            do
        //            {
        //                b = csDecrypt.ReadByte();
        //                if (b != -1)
        //                {
        //                    bytes.Add(Convert.ToByte(b));
        //                }

        //            }
        //            while (b != -1);

        //            return Encoding.Unicode.GetString(bytes.ToArray());
        //        }
        //    }
        //}

        [HttpPost]
        public ActionResult CreateUser(CreateUserDTO model)
        {
            if(!ModelState.IsValid)
            {
                model.RolesList = DataAccess.Instance.RoleAction.GetAll().Select(x => new RolesDTO
                {
                    RoleId = x.Id,
                    RoleName = x.Name
                }).ToList();
                return PartialView(model);
            }

            DataAccess.Instance.UsersAction.AddOrUpdateUser(model);
            return Json(new { success = true },JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateUserByAdminWithoutPassword(CreateUserDTO model)
        {
            if(!ModelState.IsValid)
            {
                model.RolesList = DataAccess.Instance.RoleAction.GetAll().Select(x => new RolesDTO
                {
                    RoleId = x.Id,
                    RoleName = x.Name
                }).ToList();
                return PartialView(model);
            }
            AspNetUser user = DataAccess.Instance.UsersAction.GetAll().Where(x => x.Id == model.UserId).First();
            user.Email = model.Email;
            user.UserName = model.UserName;            
            DataAccess.Instance.UsersAction.Change(user);
            DataAccess.Instance.UsersAction.AddOrUpdateUserRole(model.UserId,model.UserRoleId);

            return Json(new { success = true },JsonRequestBehavior.AllowGet);
        }

        #region Tournament

        public ActionResult Tournaments()
        {
            return View();
        }

        public ActionResult GetTournamentsList()
        {
            var data = DataAccess.Instance.TournamentAction.GetAll().Select(x => new {
                id = x.Id,
                value = x.TornamentName
            }).ToList();
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult _GetTournamentDetails(int TournamentId)
        {
            TournamentsDTO Tournament = new TournamentsDTO();
            if(TournamentId != 0)
            {
                var t = DataAccess.Instance.TournamentAction.Get(TournamentId);

                Tournament.TournamentId = TournamentId;
                Tournament.TournamentCode = t.TornamentCode;
                Tournament.TournamentName = t.TornamentName;
                Tournament.TournamentType = t.TornamentType;
                Tournament.Venue = t.Vanue;
                Tournament.StartDate = t.StartDate;
                Tournament.EndDate = t.EndDate;
                Tournament.TeamBudget = t.TeamBudget;
                Tournament.NoOfChangesAllowed = t.NbOfChangesAllowed;
                Tournament.NumberofTeams = t.NoOfTeams;
                Tournament.TeamsList = GetTeamsList(TournamentId);
                Tournament.MatchSchedules = GetSchedules(TournamentId);
            }
            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem
            {
                Text = "T20",
                Value = "T20"
            });
            listItems.Add(new SelectListItem
            {
                Text = "T20I ",
                Value = "T20I "
            });
            listItems.Add(new SelectListItem
            {
                Text = "ODI",
                Value = "ODI"
            });
            ViewBag.Options = listItems;
            return PartialView(Tournament);
        }

        private List<MatchScheduleDTO> GetSchedules(int TournamentId)
        {
            return DataAccess.Instance.MatchScheduleAction.GetAll().Where(x => x.TournamentId == TournamentId).Select(x => new MatchScheduleDTO
            {
                MatchScheduleId = x.Id,
                TeamAId = x.TeamAId,
                TeamAName = x.Team.TeamName,
                TeamBId = x.TeamBId,
                TeamBName = x.Team1.TeamName,
                ScheduleDateTime = x.ScheduleDateTime,
                Status = x.MatchStatus
            }).ToList();
        }

        private List<TeamDTO> GetTeamsList(int tournamentId)
        {
            return DataAccess.Instance.TournamentTeamAction.GetAll().Where(x => x.TournamentId == tournamentId).Select(x => new TeamDTO {
                TeamId = x.TeamId,
                TeamName = x.Team.TeamName
            }).ToList();
        }

        public ActionResult SaveTournament(TournamentsDTO model)
        {
            try
            {
                if(model.TournamentId == 0)
                {
                    Tournament t = new Tournament();
                    t.TornamentCode = model.TournamentCode;
                    t.TornamentName = model.TournamentName;
                    t.TornamentType = model.TournamentType;
                    t.Vanue = model.Venue;
                    t.StartDate = model.StartDate;
                    t.EndDate = model.EndDate;
                    t.TeamBudget = model.TeamBudget;
                    t.NbOfChangesAllowed = model.NoOfChangesAllowed;
                    t.IsActive = true;
                    t.CreateDate = DateTime.Now;
                    t.NoOfTeams = model.NumberofTeams;
                    DataAccess.Instance.TournamentAction.Add(t);
                    model.TournamentId = t.Id;

                }
                else
                {
                    Tournament t = DataAccess.Instance.TournamentAction.Get(model.TournamentId);
                    t.TornamentCode = model.TournamentCode;
                    t.TornamentName = model.TournamentName;
                    t.TornamentType = model.TournamentType;
                    t.Vanue = model.Venue;
                    t.StartDate = model.StartDate;
                    t.EndDate = model.EndDate;
                    t.TeamBudget = model.TeamBudget;
                    t.NbOfChangesAllowed = model.NoOfChangesAllowed;
                    t.IsActive = true;
                    t.NoOfTeams = model.NumberofTeams;
                    DataAccess.Instance.TournamentAction.Change(t);
                }
                return Json(new { success = true, message = "Tournament Information Saved Successfully", TournamentId = model.TournamentId }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ee)
            {
                return Json(new { success = false, message = ee }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddTeamToTournament(int TeamId, int TournamentId)
        {
            try
            {
                int[] TeamIds = DataAccess.Instance.TournamentTeamAction.GetAll().Where(x => x.TournamentId == TournamentId).Select(x => x.TeamId).ToArray();

                int[] PlayerIds = DataAccess.Instance.TeamPlayerAction.GetAll().Where(x => TeamIds.Contains(x.TeamId)).Select(x => x.PlayerId).ToArray();
                int[] SelectedTeamPlayers = DataAccess.Instance.TeamPlayerAction.GetAll().Where(x => x.TeamId == TeamId).Select(x => x.PlayerId).ToArray();

                int[] same = PlayerIds.Intersect(SelectedTeamPlayers).ToArray();


                if(same.Length >0)
                {
                    return Json(new { success = false, message = "This team contains one of same players already added in one of below teams" }, JsonRequestBehavior.AllowGet);
                }



                TournamentTeam info = new TournamentTeam();
                info.TeamId = TeamId;
                info.TournamentId = TournamentId;
                DataAccess.Instance.TournamentTeamAction.Add(info);
                return Json(new { success = true, message = "Team added to tournament successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ee)
            {
                return Json(new { success = false, message = ee }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult RemoveTeamFromTournament(int TeamId, int TournamentId)
        {
            try
            {
                var data = DataAccess.Instance.TournamentTeamAction.GetAll().Where(x => x.TeamId == TeamId && x.TournamentId == TournamentId).FirstOrDefault();

                if(data != null)
                {
                    var MatchSchedule = DataAccess.Instance.MatchScheduleAction.GetAll().Where(x => x.TournamentId == TournamentId && (x.TeamAId == TeamId || x.TeamBId == TeamId)).ToList();

                    foreach (var item in MatchSchedule)
                    {
                        var BatsmenStatics = DataAccess.Instance.BatsmenAction.GetAll().Where(x => x.MatchScheduleId == item.Id).ToList();
                        foreach (var item2 in BatsmenStatics)
                        {
                            DataAccess.Instance.BatsmenAction.Delete(item2.Id);
                        }

                        var BowlerStatics = DataAccess.Instance.BowlerStaticsAction.GetAll().Where(x => x.MatchScheduleId == item.Id).ToList();
                        foreach (var item2 in BowlerStatics)
                        {
                            DataAccess.Instance.BowlerStaticsAction.Delete(item2.Id);
                        }

                        DataAccess.Instance.MatchScheduleAction.Delete(item.Id);
                    }

                    DataAccess.Instance.TournamentTeamAction.Delete(data.Id);
                }
                return Json(new { success = true, message = "Team removed from tournament successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ee)
            {
                return Json(new { success = false, message = ee }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult CancelMatch(int ScheduleId)
        {
            try
            {
                var Schedule = DataAccess.Instance.MatchScheduleAction.Get(ScheduleId);
                DataAccess.Instance.MatchScheduleAction.Delete(Schedule.Id);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ee)
            {
                return Json(new { success = false, message = ee }, JsonRequestBehavior.AllowGet);
            }
        }


        public PartialViewResult GetListofTeamsInTournament(int TournamentId)
        {
            return PartialView("_TournamentTeamsList", GetTeamsList(TournamentId));
        }

        public ActionResult GetTeamsNotInTournament(int tournamentId)
        {
            try
            {
                int?[] TeamIds = DataAccess.Instance.TournamentTeamAction.GetAll().Where(x => x.TournamentId == tournamentId).Select(x =>(int?) x.TeamId).ToArray();

                var data = DataAccess.Instance.TeamAction.GetAll().Where(x => !TeamIds.Contains(x.Id) && x.IsVirtualTeam != true).Select(x => new TeamDTO
                {
                    TeamId = x.Id,
                    TeamName = x.TeamName
                }).ToList();
                return Json(new { data = data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ee)
            {
                return Json(new { success = false, message = ee }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddTeamsSchedule(int TeamAId, int TeamBId, DateTime MatchSchedule, int TournamentId)
        {
            try
            {
                MatchSchedule info = new MatchSchedule();
                info.TournamentId = TournamentId;
                info.TeamAId = TeamAId;
                info.TeamBId = TeamBId;
                info.ScheduleDateTime = MatchSchedule;
                DataAccess.Instance.MatchScheduleAction.Add(info);
                return Json(new { success = true, message = "Match schedule saved successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ee)
            {
                return Json(new { success = false, message = ee }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RemoveSchedule(int TeamAId, int TeamBId, int TournamentId)
        {
            try
            {
                var data = DataAccess.Instance.MatchScheduleAction.GetAll().Where(x => x.TeamAId == TeamAId && x.TeamBId == TeamBId && x.TournamentId == TournamentId).FirstOrDefault();
                if(data != null)
                {
                    DataAccess.Instance.MatchScheduleAction.Delete(data.Id);
                }
                
                return Json(new { success = true, message = "Match schedule removed successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ee)
            {
                return Json(new { success = false, message = ee }, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult _GetTournamentSchedules(int TournamentId)
        {
            return PartialView(GetSchedules(TournamentId));
        }


        public ActionResult GetTeamsWithNoSchedule(int TournamentId)
        {
            try
            {
               // int?[] TeamAIds = DataAccess.Instance.MatchScheduleAction.GetAll().Where(x => x.TournamentId == TournamentId).Select(x => (int?)x.TeamAId).ToArray();
              //  int?[] TeamBIds = DataAccess.Instance.MatchScheduleAction.GetAll().Where(x => x.TournamentId == TournamentId).Select(x => (int?)x.TeamBId).ToArray();

                var Teams = DataAccess.Instance.TournamentTeamAction.GetAll().Where(x => x.TournamentId == TournamentId ).Select(x => new {
                    TeamId = x.Team.Id,
                    TeamName = x.Team.TeamName
                }).ToList();

                return Json(new { data = Teams }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ee)
            {
                return Json(new { success = false, message = ee }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SubmitResults(int Id)
        {

            MatchScheduleDTO Schedule = GetExistingResults(Id);
            return View(Schedule);
        }


        public PartialViewResult ShowResults(int ScheduleId)
        {

            MatchScheduleDTO Schedule = GetExistingResults(ScheduleId);
            return PartialView(Schedule);
        }

        private MatchScheduleDTO GetExistingResults(int id)
        {
            MatchScheduleDTO Schedule = new MatchScheduleDTO();
            var d = DataAccess.Instance.MatchScheduleAction.Get(id);
            Schedule.MatchScheduleId = id;
            Schedule.ScheduleDate = d.ScheduleDateTime.Value.ToString("dd MMM yyyy");
            Schedule.TeamAId = d.TeamAId;
            Schedule.TeamBId = d.TeamBId;
            Schedule.TeamAName = d.Team.TeamName;
            Schedule.TeamBName = d.Team1.TeamName;
            Schedule.TournamentTitle = d.Tournament.TornamentName;
            Schedule.Location = d.Tournament.Vanue;
            Schedule.TeamAPlayers = GetPlayers(d.TeamAId);
            Schedule.TeamBPlayers = GetPlayers(d.TeamBId);

            MatchResultsDTO  result= new MatchResultsDTO();
            var model = DataAccess.Instance.MatchResultAction.GetAll().Where(x=>x.MatchScheduleId == id).FirstOrDefault();
            if(model != null)
            {
                //result.MatchId = model.MatchScheduleId != null ? model.MatchScheduleId.Value : 0;
                //result.TeamAScore = model.TeamAScore != null ? model.TeamAScore.Value : 0;
                //result.TeamAOversPlayed = model.TeamAOversPlayed;
                //result.TeamAPlayersOut = model.TeamAPlayersOut != null ? model.TeamAPlayersOut.Value : 0;
                //result.TeamBScore = model.TeamBScore != null ? model.TeamBScore.Value : 0;
                //result.TeamBOversPlayed = model.TeamBOversPlayed;
                //result.TeamBPlayersOut = model.TeamBPlayersOut != null ? model.TeamBPlayersOut.Value : 0;
                //result.Winner = model.WinnerId != null ? model.WinnerId.Value : 0;
                //result.TeamAExtras = model.TeamAExtras;
                //result.TeamADidntBat = model.TeamADidntBat;
                //result.TeamAFallofWickets = model.TeamAFallofWickets;
                //result.TeamBExtras = model.TeamBExtras;
                //result.TeamADidntBat = model.TeamADidntBat;
                //result.TeamBFallofWickets = model.TeamBFallofWickets;
                //result.WinCondition = model.WonBy;

                ViewBag.MatchResult = model;

                List<MatchDetailsDTO> TotalInningRecords = DataAccess.Instance.BatsmenAction.GetAll().AsEnumerable().Where(x => x.MatchScheduleId == id)
                    .Select(x => new MatchDetailsDTO
                    {
                        PlayerName = DataAccess.Instance.PlayersActions.Get(x.PlayerId).PlayerName,
                        OutType = x.OutType,
                        OutByPlayerName = x.OutByPlayerId != null ? DataAccess.Instance.PlayersActions.Get(x.OutByPlayerId.Value).PlayerName : string.Empty,
                        Runs = x.TotalRuns,
                        Balls = x.TotalBalls,
                        Minutes = x.MinutesSpend,
                        Fours = x.Fours,
                        Sixes = x.Sixes,
                        Inning = x.Inning != null ? x.Inning.Value : 0
                    }).ToList();

                List<MatchDetailsDTO> TotalBowlerRecords = DataAccess.Instance.BowlerStaticsAction.GetAll().AsEnumerable().Where(x => x.MatchScheduleId == id)
                    .Select(x => new MatchDetailsDTO
                    {
                        PlayerName = DataAccess.Instance.PlayersActions.Get(x.PlayerId).PlayerName,
                        Overs = x.Overs,
                        Maiden = x.MaidenOvers,
                        Runs = x.RunsGiven,
                        Wickets = x.WicketsTaken,
                        Economy = x.Economy,
                        Fours = x.Fours != null ? x.Fours.Value: 0,
                        Sixes = x.Sixes != null ? x.Sixes.Value : 0,
                        Wides = x.WideBalls,
                        NoBalls = x.NoBalls,
                        Inning = x.Inning != null ? x.Inning.Value : 0
                    }).ToList();

                List<MatchDetailsDTO> TeamAInningDetails = new List<MatchDetailsDTO>();
                List<MatchDetailsDTO> TeamBInningDetails = new List<MatchDetailsDTO>();
                List<MatchDetailsDTO> TeamABowlingDetails = new List<MatchDetailsDTO>();
                List<MatchDetailsDTO> TeamBBowlingDetails = new List<MatchDetailsDTO>();


                TeamAInningDetails = TotalInningRecords.Where(x => x.Inning == 1).ToList();
                TeamBBowlingDetails = TotalBowlerRecords.Where(x => x.Inning == 1).ToList();

                TeamBInningDetails = TotalInningRecords.Where(x => x.Inning == 2).ToList();                
                TeamABowlingDetails = TotalBowlerRecords.Where(x => x.Inning == 2).ToList();

                ViewBag.TeamAInningDetails = TeamAInningDetails;
                ViewBag.TeamBBowlingDetails = TeamBBowlingDetails;
                ViewBag.TeamBInningDetails = TeamBInningDetails;
                ViewBag.TeamABowlingDetails = TeamABowlingDetails;


            }
            return Schedule;
        }

        private List<PlayersDto> GetPlayers(int teamAId)
        {
            return DataAccess.Instance.TeamPlayerAction.GetAll().Where(x => x.TeamId == teamAId).Select(x => new PlayersDto {
                PlayerId = x.PlayerId,
                PlayerName = x.Player.PlayerName
            }).ToList();
        }

        [HttpPost]
        public ActionResult SubmitMatchResults(MatchResultsDTO model)
        {
            try
            {
                List<MatchDetailsDTO> TeamAInningDetails = new List<MatchDetailsDTO>();
                List<MatchDetailsDTO> TeamBInningDetails = new List<MatchDetailsDTO>();
                List<MatchDetailsDTO> TeamABowlingDetails = new List<MatchDetailsDTO>();
                List<MatchDetailsDTO> TeamBBowlingDetails = new List<MatchDetailsDTO>();

                List<VirtualTeamPlayer> l_listPlayers = DataAccess.Instance.VirtualPlayerAction.GetAll().ToList();

                DataAccess.Instance.MatchScheduleAction.DeleteResults(model.MatchId);
                if (!string.IsNullOrEmpty(model.TeamAInningDetails))
                {
                    TeamAInningDetails =JsonConvert.DeserializeObject<List<MatchDetailsDTO>>(model.TeamAInningDetails);
                    foreach (var item in TeamAInningDetails)
                    {
                        var PlayerRecord = DataAccess.Instance.PlayersActions.Get(item.PlayerId);
                        BatsmenStatistic statics = new BatsmenStatistic();
                        statics.MatchScheduleId = model.MatchId;
                        statics.PlayerId = item.PlayerId;
                        statics.Inning = 1;
                        statics.PlayerType = PlayerRecord.PlayerType;
                        statics.TotalRuns = item.Runs;
                        statics.TotalBalls = item.Balls;
                        statics.MinutesSpend = item.Minutes;
                        statics.Fours = item.Fours;
                        statics.Sixes = item.Sixes;
                        //each six 10, four 5, single run 1 poinmt
                        int l_intTotalPoints = (item.Sixes * 10) + (item.Fours * 5) - ((item.Runs - ((item.Sixes*6) + (item.Fours*4))));
                        //50 30,100 60points,3 sixes 10 and 5sixs 20 points
                        if (item.Runs > 99)
                        {
                            l_intTotalPoints += 60;
                        }
                        else
                        {
                            if (item.Runs > 49)
                            {
                                l_intTotalPoints += 30;
                            }
                        }
                        if (item.Sixes > 4)
                        {
                            l_intTotalPoints += 20;
                        }
                        else
                        {
                            if (item.Sixes > 2)
                            {
                                l_intTotalPoints += 10;
                            }
                        }
                        if(model.TeamAScore > model.TeamBScore)
                        {
                            l_intTotalPoints += 5;
                        }
                        else
                        {
                            l_intTotalPoints -= 5;
                        }
                        if (item.Minutes > 179)
                        {
                            l_intTotalPoints += 30;
                        }
                        else
                        {
                            if(item.Minutes > 149)
                            {
                                l_intTotalPoints += 20;
                            }
                            else
                            {
                                if (item.Minutes > 109)
                                {
                                    l_intTotalPoints += 10;
                                }
                                else
                                {
                                    if (item.Minutes > 89)
                                    {
                                        l_intTotalPoints += 0;
                                    }
                                    else
                                    {
                                        if (item.Minutes > 69)
                                        {
                                            l_intTotalPoints -= 10;
                                        }
                                        else
                                        {
                                                l_intTotalPoints -= 20;
                                        }
                                    }
                                }
                            }
                        }
                        foreach (var SingleVP in l_listPlayers)
                        {
                            if(SingleVP.PlayerId == item.PlayerId)
                            {
                                if (SingleVP.IsManOfMatch == true)
                                {
                                    l_intTotalPoints += 20;
                                }
                                if (SingleVP.IsCaptain == true)
                                {
                                    l_intTotalPoints += l_intTotalPoints;
                                }
                                break;
                            }
                        }
                        statics.Points = l_intTotalPoints;
                        if(item.OutStatus == "Out" && item.OutByPlayerId != 0)
                        {
                            statics.OutByPlayerId = item.OutByPlayerId;                             
                        }

                        statics.OutType = item.OutType;
                        PlayerRecord.TotalPoints += l_intTotalPoints;
                        DataAccess.Instance.PlayersActions.Change(PlayerRecord);
                        DataAccess.Instance.BatsmenAction.Add(statics);
                    }
                }

                if (!string.IsNullOrEmpty(model.TeamBInningDetails))
                {
                    TeamBInningDetails = JsonConvert.DeserializeObject<List<MatchDetailsDTO>>(model.TeamBInningDetails);
                    foreach (var item in TeamBInningDetails)
                    {
                        var PlayerRecord = DataAccess.Instance.PlayersActions.Get(item.PlayerId);
                        BatsmenStatistic statics = new BatsmenStatistic();
                        statics.MatchScheduleId = model.MatchId;
                        statics.PlayerId = item.PlayerId;
                        statics.Inning = 2;
                        statics.PlayerType = PlayerRecord.PlayerType;
                        statics.TotalRuns = item.Runs;
                        statics.TotalBalls = item.Balls;
                        statics.MinutesSpend = item.Minutes;
                        statics.Fours = item.Fours;
                        statics.Sixes = item.Sixes;
                        //each six 10, four 5, single run 1 poinmt
                        int l_intTotalPoints = (item.Sixes * 10) + (item.Fours * 5) - ((item.Runs - ((item.Sixes * 6) + (item.Fours * 4))));
                        //50 30,100 60points,3 sixes 10 and 5sixs 20 points
                        if (item.Runs > 99)
                        {
                            l_intTotalPoints += 60;
                        }
                        else
                        {
                            if (item.Runs > 49)
                            {
                                l_intTotalPoints += 30;
                            }
                        }
                        if (item.Sixes > 4)
                        {
                            l_intTotalPoints += 20;
                        }
                        else
                        {
                            if (item.Sixes > 2)
                            {
                                l_intTotalPoints += 10;
                            }
                        }
                        if (model.TeamAScore > model.TeamBScore)
                        {
                            l_intTotalPoints += 5;
                        }
                        else
                        {
                            l_intTotalPoints -= 5;
                        }

                        if (item.Minutes > 179)
                        {
                            l_intTotalPoints += 30;
                        }
                        else
                        {
                            if (item.Minutes > 149)
                            {
                                l_intTotalPoints += 20;
                            }
                            else
                            {
                                if (item.Minutes > 109)
                                {
                                    l_intTotalPoints += 10;
                                }
                                else
                                {
                                    if (item.Minutes > 89)
                                    {
                                        l_intTotalPoints += 0;
                                    }
                                    else
                                    {
                                        if (item.Minutes > 69)
                                        {
                                            l_intTotalPoints -= 10;
                                        }
                                        else
                                        {
                                            l_intTotalPoints -= 20;
                                        }
                                    }
                                }
                            }
                        }
                        foreach (var SingleVP in l_listPlayers)
                        {
                            if (SingleVP.PlayerId == item.PlayerId)
                            {
                                if (SingleVP.IsManOfMatch == true)
                                {
                                    l_intTotalPoints += 20;
                                }
                                if (SingleVP.IsCaptain == true)
                                {
                                    l_intTotalPoints += l_intTotalPoints;
                                }
                                break;
                            }
                        }
                        statics.Points = l_intTotalPoints;

                        if (item.OutType == "Out" && item.OutByPlayerId != 0)
                        {
                            statics.OutByPlayerId = item.OutByPlayerId;
                        }
                        PlayerRecord.TotalPoints += l_intTotalPoints;
                        DataAccess.Instance.PlayersActions.Change(PlayerRecord);
                        statics.OutType = item.OutType;
                        DataAccess.Instance.BatsmenAction.Add(statics);
                    }
                }

                if (!string.IsNullOrEmpty(model.TeamABowlingDetails))
                {
                    TeamABowlingDetails = JsonConvert.DeserializeObject<List<MatchDetailsDTO>>(model.TeamABowlingDetails);

                    foreach (var item in TeamABowlingDetails)
                    {
                        var PlayerRecord = DataAccess.Instance.PlayersActions.Get(item.PlayerId);
                        BowlerStatistic statics = new BowlerStatistic();
                        statics.MatchScheduleId = model.MatchId;
                        statics.PlayerId = item.PlayerId;
                        statics.Overs = item.Overs;
                        statics.Inning = 2;
                        statics.MaidenOvers = item.Maiden;
                        statics.RunsGiven = item.Runs;
                        statics.WicketsTaken = item.Wickets;
                        statics.Economy = item.Economy;
                        statics.Fours = item.Fours;
                        statics.Sixes = item.Sixes;
                        statics.WideBalls = item.Wides;
                        statics.NoBalls = item.NoBalls;

                        int l_intTotalPoints = 0;
                        l_intTotalPoints = (item.Wickets * 15) + (item.Maiden * 20);
                        if (item.Wickets > 4)
                        {
                            l_intTotalPoints += 60;
                        }
                        else
                        {
                            if (item.Runs > 2)
                            {
                                l_intTotalPoints += 30;
                            }
                        }
                        if (item.Economy > 8)
                        {
                            l_intTotalPoints -= 20;
                        }
                        else
                        {
                            if(item.Economy > 7)
                            {
                                l_intTotalPoints -= 10;
                            }
                            else
                            {
                                if (item.Economy > 6)
                                {
                                    l_intTotalPoints += 0;
                                }
                                else
                                {
                                    if (item.Economy > 5)
                                    {
                                        l_intTotalPoints += 10;
                                    }
                                    else
                                    {
                                        if (item.Economy > 3)
                                        {
                                            l_intTotalPoints += 20;
                                        }
                                        else
                                        {
                                            if (item.Economy > 0)
                                            {
                                                l_intTotalPoints += 30;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        //strike rate calculations
                        //issue will be discussed with aamir
                        foreach (var SingleVP in l_listPlayers)
                        {
                            if (SingleVP.PlayerId == item.PlayerId)
                            {
                                if (SingleVP.IsManOfMatch == true)
                                {
                                    l_intTotalPoints += 20;
                                }
                                if (SingleVP.IsCaptain == true)
                                {
                                    l_intTotalPoints += l_intTotalPoints;
                                }
                                break;
                            }
                        }
                        statics.Points = l_intTotalPoints;
                        PlayerRecord.TotalPoints = l_intTotalPoints;
                        DataAccess.Instance.PlayersActions.Change(PlayerRecord);
                        DataAccess.Instance.BowlerStaticsAction.Add(statics);
                    }
                }

                if (!string.IsNullOrEmpty(model.TeamBBowlingDetails))
                {
                    TeamBBowlingDetails = JsonConvert.DeserializeObject<List<MatchDetailsDTO>>(model.TeamBBowlingDetails);

                    foreach (var item in TeamBBowlingDetails)
                    {
                        var PlayerRecord = DataAccess.Instance.PlayersActions.Get(item.PlayerId);
                        BowlerStatistic statics = new BowlerStatistic();
                        statics.MatchScheduleId = model.MatchId;
                        statics.PlayerId = item.PlayerId;
                        statics.Overs = item.Overs;
                        statics.Inning = 1;
                        statics.MaidenOvers = item.Maiden;
                        statics.RunsGiven = item.Runs;
                        statics.WicketsTaken = item.Wickets;
                        statics.Economy = item.Economy;
                        statics.Fours = item.Fours;
                        statics.Sixes = item.Sixes;
                        statics.WideBalls = item.Wides;
                        statics.NoBalls = item.NoBalls;
                        int l_intTotalPoints = 0;
                        l_intTotalPoints = (item.Wickets * 15) + (item.Maiden * 20);
                        if (item.Wickets > 4)
                        {
                            l_intTotalPoints += 60;
                        }
                        else
                        {
                            if (item.Runs > 2)
                            {
                                l_intTotalPoints += 30;
                            }
                        }
                        if (item.Economy > 8)
                        {
                            l_intTotalPoints -= 20;
                        }
                        else
                        {
                            if (item.Economy > 7)
                            {
                                l_intTotalPoints -= 10;
                            }
                            else
                            {
                                if (item.Economy > 6)
                                {
                                    l_intTotalPoints += 0;
                                }
                                else
                                {
                                    if (item.Economy > 5)
                                    {
                                        l_intTotalPoints += 10;
                                    }
                                    else
                                    {
                                        if (item.Economy > 3)
                                        {
                                            l_intTotalPoints += 20;
                                        }
                                        else
                                        {
                                            if (item.Economy > 0)
                                            {
                                                l_intTotalPoints += 30;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //strike rate calculations
                        //issue will be discussed with aamir
                        foreach (var SingleVP in l_listPlayers)
                        {
                            if (SingleVP.PlayerId == item.PlayerId)
                            {
                                if (SingleVP.IsManOfMatch == true)
                                {
                                    l_intTotalPoints += 20;
                                }
                                if (SingleVP.IsCaptain == true)
                                {
                                    l_intTotalPoints += l_intTotalPoints;
                                }
                                break;
                            }
                        }
                        PlayerRecord.TotalPoints = l_intTotalPoints;
                        DataAccess.Instance.PlayersActions.Change(PlayerRecord);
                        statics.Points = l_intTotalPoints;
                        DataAccess.Instance.BowlerStaticsAction.Add(statics);
                    }
                }


                MatchResult result = DataAccess.Instance.MatchResultAction.GetAll().Where(x => x.MatchScheduleId == model.MatchId).FirstOrDefault();
                if(result != null)
                {
                    result.MatchScheduleId = model.MatchId;
                    result.TeamAScore = model.TeamAScore;
                    result.TeamAOversPlayed = model.TeamAOversPlayed;
                    result.TeamAPlayersOut = model.TeamAPlayersOut;
                    result.TeamBScore = model.TeamBScore;
                    result.TeamBOversPlayed = model.TeamBOversPlayed;
                    result.TeamBPlayersOut = model.TeamBPlayersOut;
                    result.WinnerId = model.Winner;
                    result.TeamAExtras = model.TeamAExtras;
                    result.TeamADidntBat = model.TeamADidntBat;
                    result.TeamAFallofWickets = model.TeamAFallofWickets;
                    result.TeamBExtras = model.TeamBExtras;
                    result.TeamBDidntBat = model.TeamADidntBat;
                    result.TeamBFallofWickets = model.TeamBFallofWickets;
                    result.WonBy = model.WinCondition;
                    DataAccess.Instance.MatchResultAction.Change(result); 
                }
                else
                {
                    result = new MatchResult();
                    result.MatchScheduleId = model.MatchId;
                    result.TeamAScore = model.TeamAScore;
                    result.TeamAOversPlayed = model.TeamAOversPlayed;
                    result.TeamAPlayersOut = model.TeamAPlayersOut;
                    result.TeamBScore = model.TeamBScore;
                    result.TeamBOversPlayed = model.TeamBOversPlayed;
                    result.TeamBPlayersOut = model.TeamBPlayersOut;
                    result.WinnerId = model.Winner;
                    result.TeamAExtras = model.TeamAExtras;
                    result.TeamADidntBat = model.TeamADidntBat;
                    result.TeamAFallofWickets = model.TeamAFallofWickets;
                    result.TeamBExtras = model.TeamBExtras;
                    result.TeamBDidntBat = model.TeamADidntBat;
                    result.TeamBFallofWickets = model.TeamBFallofWickets;
                    result.WonBy = model.WinCondition;
                    DataAccess.Instance.MatchResultAction.Add(result);
                }


                var Schedule = DataAccess.Instance.MatchScheduleAction.Get(model.MatchId);
                Schedule.MatchStatus = "Result Submitted";
                DataAccess.Instance.MatchScheduleAction.Change(Schedule);

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ee)
            {
                return Json(new { success = false, message = ee }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteTournament(int TournamentId)
        {
            try
            {
                int[] TournamentTeams = DataAccess.Instance.TournamentTeamAction.GetAll().Where(x => x.TournamentId == TournamentId).Select(x => x.Id).ToArray();
                foreach (var Id in TournamentTeams)
                {
                    DataAccess.Instance.TournamentTeamAction.Delete(Id);
                }

                int[] MatchSchedules = DataAccess.Instance.MatchScheduleAction.GetAll().Where(x => x.TournamentId == TournamentId).Select(x => x.Id).ToArray();
                foreach (int schedule in MatchSchedules)
                {
                    int[] BatsmenData = DataAccess.Instance.BatsmenAction.GetAll().Where(x => x.MatchScheduleId == schedule).Select(x => x.Id).ToArray();
                    foreach (var batsman in BatsmenData)
                    {
                        DataAccess.Instance.BatsmenAction.Delete(batsman);
                    }

                    int[] BowlerData = DataAccess.Instance.BowlerStaticsAction.GetAll().Where(x => x.Id == schedule).Select(x => x.Id).ToArray();
                    foreach (var Bowler in BowlerData)
                    {
                        DataAccess.Instance.BowlerStaticsAction.Delete(Bowler);
                    }

                    DataAccess.Instance.MatchScheduleAction.Delete(schedule);
                }

                DataAccess.Instance.TournamentAction.Delete(TournamentId);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ee)
            {
                return Json(new { success = false, message = ee }, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region Player Management

        public ActionResult Players()
        {
            List<PlayersDto> PlayersList = DataAccess.Instance.PlayersActions.GetAll().AsEnumerable().Select(x => new PlayersDto {
                PlayerId = x.Id,
                PlayerName = x.PlayerName,
                PlayerType = x.PlayerType,
                TeamName = x.TeamPlayers.FirstOrDefault() != null ? x.TeamPlayers.FirstOrDefault().Team.TeamName : string.Empty,
                Tournament = x.TeamPlayers.FirstOrDefault() != null ? GetTournamentName(x.TeamPlayers.FirstOrDefault().Id) : string.Empty
            }).ToList();
            return View(PlayersList);
        }

        private string GetTournamentName(int? team_Id)
        {
            if( team_Id != null)
            {
                var data = DataAccess.Instance.TournamentTeamAction.GetAll().Where(x => x.TeamId == team_Id).FirstOrDefault();

                if(data != null)
                {
                    return data.Tournament.TornamentName;
                }
            }

            return string.Empty;
        }

        public ActionResult ManagePlayer(int PlayerId)
        {
            PlayersDto player = new PlayersDto();
            if (PlayerId != 0)
            {
                player = GetPlayerInfo(PlayerId);
            }

            if(player.StaticsList.Count == 0)
            {
                List<PlayerStatics> ListOFStatics = new List<DataTransferObjects.Models.PlayerStatics>();
                PlayerStatics statics = new PlayerStatics();
                statics.MatchType = "T20s";
                ListOFStatics.Add(statics);

                PlayerStatics statics2 = new PlayerStatics();
                statics2.MatchType = "T20Is";
                ListOFStatics.Add(statics2);

                PlayerStatics statics3 = new PlayerStatics();
                statics3.MatchType = "ODIs";
                ListOFStatics.Add(statics3);

                player.StaticsList = ListOFStatics;
            }

            return View(player);
        }

        public PlayersDto GetPlayerInfo(int PlayerId)
        {
            PlayersDto player = new PlayersDto();
            List<PlayerStatics> ListOFStatics = new List<DataTransferObjects.Models.PlayerStatics>(); 
            var PlayerInfo = DataAccess.Instance.PlayersActions.Get(PlayerId);
            player.PlayerId = PlayerInfo.Id;
            player.PlayerName = PlayerInfo.PlayerName;
            player.PlayerType = PlayerInfo.PlayerType;
            player.PlayerValue = PlayerInfo.Value;
            string path = Server.MapPath("~/PlayerImages/" + PlayerId+".jpg");
            if(System.IO.File.Exists(path))
            {
                player.HasImage = true;
            }
            var PlayerStatics = DataAccess.Instance.PlayerStaticsAction.GetAll().Where(x => x.PlayerId == PlayerId).ToList();

            foreach (var item in PlayerStatics)
            {
                PlayerStatics statics = new PlayerStatics();
                statics.MatchType = item.MatchType;

                StaticsList list = new StaticsList();
                list.TotalMatches = item.TotalMatches;
                list.InningsBatted = item.InningsBatted;
                list.RunsScored = item.RunsScored;
                list.Average = item.Average;
                list.Hundred = item.Hundred;
                list.Fifty = item.Fifty;
                list.Fours = item.Fours;
                list.Sixes = item.Sixes;
                list.BallsBowled = item.BallsBowled;
                list.WicketsTaken = item.WicketsTaken;
                list.Economy = item.Economy;
                list.FourWicketsInMatch = item.FourWicketsInMatch;
                list.FiveWicketsInMatch = item.FiveWicketsInMatch;
                list.TenWicketsInMatch = item.TenWicketsInMatch;
                list.HighestScore = item.HighestScore;
                list.InningsBowled = item.InningsBowled;
                list.NotOuts = item.NotOuts;
                list.BallsFaced = item.BallsFaced;
                list.StrikeRate = item.StrikeRate;
                list.CatchesTaken = item.CatchesTaken;
                list.RunsGiven = item.RunsGiven; 
                statics.Statics = list;
                ListOFStatics.Add(statics);

            }

            player.StaticsList = ListOFStatics;

            return player;
        }

        [HttpPost]
        public ActionResult SavePlayerDetails(int PlayerId, string PlayerName, int PlayerValue, string PlayerType, string Statics)
        {
            try
            {

                if(PlayerId == 0)
                {
                    Player player = new Player();
                    player.PlayerName = PlayerName;
                    player.PlayerType = PlayerType;
                    player.Value = PlayerValue;
                    player.IsActive = true;
                    DataAccess.Instance.PlayersActions.Add(player);
                    PlayerId = player.Id;
                }
                else
                {
                    Player player = DataAccess.Instance.PlayersActions.GetAll().Where(x=>x.Id== PlayerId).FirstOrDefault();
                    player.PlayerName = PlayerName;
                    player.PlayerType = PlayerType;
                    player.IsActive = true;
                    player.Value = PlayerValue;
                    DataAccess.Instance.PlayersActions.Change(player);
                }

                if (Request.Files.Count > 0)
                {
                    foreach (string file2 in Request.Files)
                    {
                        var _file = Request.Files[file2];
                        var path = Path.Combine(Server.MapPath("~/PlayerImages/"), PlayerId+".jpg");
                        _file.SaveAs(path);
                    }
                }

                DataAccess.Instance.PlayerStaticsAction.DeleteStatics(PlayerId);

               List<StaticsList> list =  JsonConvert.DeserializeObject<List<StaticsList>>(Statics);

                foreach (StaticsList statics in list)
                {
                    PlayerStatic ps = new PlayerStatic();
                    ps.PlayerId = PlayerId;
                    ps.MatchType = statics.MatchType;
                    ps.TotalMatches = statics.TotalMatches;
                    ps.InningsBatted = statics.InningsBatted;
                    ps.RunsScored = statics.RunsScored;
                    ps.Average = statics.Average;
                    ps.Hundred = statics.Hundred;
                    ps.Fifty = statics.Fifty;
                    ps.Fours = statics.Fours;
                    ps.Sixes = statics.Sixes;
                    ps.BallsBowled = statics.BallsBowled;
                    ps.WicketsTaken = statics.WicketsTaken;
                    ps.Economy = statics.Economy;
                    ps.FourWicketsInMatch = statics.FourWicketsInMatch;
                    ps.FiveWicketsInMatch = statics.FiveWicketsInMatch;
                    ps.HighestScore = statics.HighestScore;
                    ps.InningsBowled = statics.InningsBowled;
                    ps.NotOuts = statics.NotOuts;
                    ps.BallsFaced = statics.BallsFaced;
                    ps.StrikeRate = statics.StrikeRate;
                    ps.CatchesTaken = statics.CatchesTaken;
                    ps.RunsGiven = statics.RunsGiven;
                    DataAccess.Instance.PlayerStaticsAction.Add(ps);
                }


                return Json(new { success = true, message = "Player details saved successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ee)
            {
                return Json(new { success = false, message = ee }, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult ShowPlayerProfile(int PlayerId)
        {
            PlayersDto player = new PlayersDto();
            if (PlayerId != 0)
            {
                player = GetPlayerInfo(PlayerId);
            }
            else
            {
                List<PlayerStatics> ListOFStatics = new List<DataTransferObjects.Models.PlayerStatics>();
                PlayerStatics statics = new PlayerStatics();
                statics.MatchType = "T20s";
                ListOFStatics.Add(statics);

                PlayerStatics statics2 = new PlayerStatics();
                statics2.MatchType = "T20Is";
                ListOFStatics.Add(statics2);

                PlayerStatics statics3 = new PlayerStatics();
                statics3.MatchType = "ODIs";
                ListOFStatics.Add(statics3);

                player.StaticsList = ListOFStatics;
            }

            return PartialView(player);
        }

        public ActionResult DeletePlayer(int PlayerId)
        {
           try
            {
                int[] PlayerStatics = DataAccess.Instance.PlayerStaticsAction.GetAll().Where(x => x.PlayerId == PlayerId).Select(x => x.Id).ToArray();
                foreach (int statics in PlayerStatics)
                {
                    DataAccess.Instance.PlayerStaticsAction.Delete(statics);
                }

                int[] BowlerStatics = DataAccess.Instance.BowlerStaticsAction.GetAll().Where(x => x.PlayerId == PlayerId).Select(x => x.Id).ToArray();
                foreach (int statics in BowlerStatics)
                {
                    DataAccess.Instance.BowlerStaticsAction.Delete(statics);
                }

                int[] BatsmenStatics = DataAccess.Instance.BatsmenAction.GetAll().Where(x => x.PlayerId == PlayerId).Select(x => x.Id).ToArray();
                foreach (int statics in BatsmenStatics)
                {
                    DataAccess.Instance.BatsmenAction.Delete(statics);
                }

                int[] TeamPlayers = DataAccess.Instance.TeamPlayerAction.GetAll().Where(x => x.PlayerId == PlayerId).Select(x => x.Id).ToArray();
                foreach (var tm in TeamPlayers)
                {
                    DataAccess.Instance.TeamPlayerAction.Delete(tm);
                }

                DataAccess.Instance.PlayersActions.Delete(PlayerId);
                return Json(new { success= true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ee)
            {
                return Json(new { success = false, message = ee }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ValidatePlayerName(int PlayerId, string PlayerName)
        {
            bool DoesNotExist = false;
            if(PlayerId == 0)
            {
                DoesNotExist = DataAccess.Instance.PlayersActions.GetAll().Where(x => x.PlayerName == PlayerName).Any();
            }
            else
            {
                DoesNotExist = DataAccess.Instance.PlayersActions.GetAll().Where(x => x.PlayerName == PlayerName && x.Id != PlayerId).Any();
            }

            return Json(new { success = !DoesNotExist }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Team Management

        [HttpPost]
        public ActionResult AddToTeam(int PlayerId, int TeamId)
        {
            TeamPlayer TeamPlayer = new TeamPlayer();
            TeamPlayer.TeamId = TeamId;
            TeamPlayer.PlayerId = PlayerId;
            DataAccess.Instance.TeamPlayerAction.Add(TeamPlayer);
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RemoveFromTeam(int PlayerId, int TeamId)
        {
            TeamPlayer TeamPlayer = DataAccess.Instance.TeamPlayerAction.GetAll().Where(x => x.PlayerId == PlayerId && x.TeamId == TeamId).FirstOrDefault();
            if(TeamPlayer != null)
            {
                DataAccess.Instance.TeamPlayerAction.Delete(TeamPlayer.Id);
            }

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        
        public PartialViewResult ViewTeamPlayers(int TeamId)
        {
            List<PlayersDto> PlayersList = DataAccess.Instance.TeamPlayerAction.GetAll().Where(x => x.TeamId == TeamId).Select(x => new PlayersDto {
                PlayerName = x.Player.PlayerName
            }).ToList();
            return PartialView(PlayersList);
        }
         
        public PartialViewResult ViewVirtualTeamPlayers(int TeamId)
        {
            List<PlayersDto> PlayersList = DataAccess.Instance.VirtualPlayerAction.GetAll().Where(x => x.TeamId == TeamId).Select(x => new PlayersDto {
                PlayerName = x.Player.PlayerName
            }).ToList();
            return PartialView(PlayersList);
        }

        public ActionResult DeleteTeam(int TeamId)
        {
         try
            {
                int[] TournamentTeams = DataAccess.Instance.TournamentTeamAction.GetAll().Where(x => x.TeamId == TeamId).Select(x => x.Id).ToArray();
                foreach (var Id in TournamentTeams)
                {
                    DataAccess.Instance.TournamentTeamAction.Delete(Id);
                }

                int[] MatchSchedules = DataAccess.Instance.MatchScheduleAction.GetAll().Where(x => x.TeamAId == TeamId || x.TeamBId == TeamId).Select(x => x.Id).ToArray();
                foreach (int schedule in MatchSchedules)
                {
                    int[] BatsmenData = DataAccess.Instance.BatsmenAction.GetAll().Where(x => x.MatchScheduleId == schedule).Select(x => x.Id).ToArray();
                    foreach (var batsman in BatsmenData)
                    {
                        DataAccess.Instance.BatsmenAction.Delete(batsman);
                    }

                    int[] BowlerData = DataAccess.Instance.BowlerStaticsAction.GetAll().Where(x => x.Id == schedule).Select(x => x.Id).ToArray();
                    foreach (var Bowler in BowlerData)
                    {
                        DataAccess.Instance.BowlerStaticsAction.Delete(Bowler);
                    }

                    DataAccess.Instance.MatchScheduleAction.Delete(schedule);
                }

                int[] TeamPlayers = DataAccess.Instance.TeamPlayerAction.GetAll().Where(x => x.TeamId == TeamId).Select(x => x.Id).ToArray();
                foreach (int player in TeamPlayers)
                {
                    DataAccess.Instance.TeamPlayerAction.Delete(player);
                }

                DataAccess.Instance.TeamAction.Delete(TeamId);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch( Exception ee)
            {
                return Json(new { success = false, message= ee }, JsonRequestBehavior.AllowGet);
            }
            
        }


        #endregion

        #region Image Upload
        [HttpPost]
        public string FileUpload(HttpPostedFileBase file)
        {
            string path = "";
            string pic = "";
            if (file != null)
            {
                pic = System.IO.Path.GetFileName(file.FileName);
                path = System.IO.Path.Combine(
                                       Server.MapPath("~/UserImages"), pic);
                // file is uploaded
                file.SaveAs(path);

                // save the image path path to the database or you can send image 
                // directly to database
                // in-case if you want to store byte[] ie. for DB
                using (MemoryStream ms = new MemoryStream())
                {
                    file.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
                var UserId = User.Identity.GetUserId();
                var l_oUser = DataAccess.Instance.UsersAction.GetAll().Where(x => x.Id == UserId).First();
                l_oUser.HasImage = true;
                l_oUser.ImageURL = "~/UserImages/" + pic;
                DataAccess.Instance.UsersAction.Change(l_oUser);
            }
            ViewBag.ImageURL = "~/UserImages/" + pic;
            // after successfully uploading redirect the user
            return "/UserImages/" + pic;
        }
        [HttpGet]
        public ActionResult GetLoggedInUserImage()
        {
            var user = User.Identity.GetUserId();
            var aspNetUser = DataAccess.Instance.UsersAction.GetAll().Where(x => x.Id == user).First();
            ViewBag.HasImage = aspNetUser.HasImage;
            if (ViewBag.HasImage)
            {
                ViewBag.ImageURL = aspNetUser.ImageURL;
                return Json(new { success = true, message = aspNetUser.ImageURL }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, message = aspNetUser.ImageURL }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}