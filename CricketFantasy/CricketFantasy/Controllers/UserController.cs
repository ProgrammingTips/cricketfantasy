﻿using CricketFantasy.DataLayer;
using CricketFantasy.DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using CricketFantasy.DataTransferObjects.Models;
using System.Net.Mail;
using System.Net;

namespace CricketFantasy.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            //List<LeaguesDTO> leagues = DataAccess.Instance.LeagueAction.GetAll().AsEnumerable().Select(x => new LeaguesDTO
            //{
            //    LeagueId = x.Id,
            //    LeagueTitle = x.LeagueName,
            //    TotalTeams = DataAccess.Instance.LeagueTeamAction.GetAll().Where(y => y.LeagueId == x.Id).Count(),
            //}).OrderBy(x => x.TotalTeams).ToList();
            return View();
        }

        [AllowAnonymous]
        public ActionResult PointsCalculation()
        {
            return View();
        }

        public ActionResult Play()
        {
            return View();
        }
        //[HttpPost]
        //public ActionResult GetTeamsInTournament(string p_strTournamentID)
        //{
        //    try
        //    {
        //        int l_intTournamentID = 0;
        //        if (!string.IsNullOrEmpty(p_strTournamentID)) {
        //            l_intTournamentID = int.Parse(p_strTournamentID);
        //        }
        //        string UserId = User.Identity.GetUserId();
        //        int TeamId = 0;
        //        var TeamInformation = DataAccess.Instance.TeamAction.GetAll().Where(x => x.CreateUser == UserId).FirstOrDefault();
        //        if (TeamInformation != null)
        //        {
        //            ViewBag.TeamId = TeamInformation.Id;
        //            ViewBag.TeamName = TeamInformation.TeamName;
        //            ViewBag.VisibilityClass = "";
        //            TeamId = TeamInformation.Id;
        //        }
        //        else
        //        {
        //            ViewBag.TeamId = 0;
        //            ViewBag.TeamName = string.Empty;
        //            ViewBag.VisibilityClass = "HideItem";
        //        }
        //        IEnumerable<SelectListItem> TeamsList;
        //        if (l_intTournamentID>0)
        //        {
        //            var TeamIds = DataAccess.Instance.TournamentTeamAction.GetAll().Where(x => x.TournamentId == l_intTournamentID).Select(x => x.TeamId).ToArray();
        //            TeamsList = DataAccess.Instance.TeamAction.GetAll().Where(x => TeamIds.Contains(x.Id) && x.IsVirtualTeam != true)
        //                .Select(x => new SelectListItem
        //                {
        //                    Value = x.Id.ToString(),
        //                    Text = x.TeamName
        //                }).ToList();
        //        }
        //        else
        //        {
        //            TeamsList = DataAccess.Instance.TeamAction.GetAll().Where(x => x.IsVirtualTeam != true)
        //               .Select(x => new SelectListItem
        //               {
        //                   Value = x.Id.ToString(),
        //                   Text = x.TeamName
        //               }).ToList();
        //        }

        //        ViewBag.TeamsList = TeamsList;

        //        ViewBag.PlayersList = AvailablePlayersList("Batsman", "0", TeamId);
        //        ViewBag.TeamPlayersList = GetPlayersList(TeamId);
        //        return Json(new { success = true, data = ViewBag.TeamsList }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        public ActionResult LeagueTeamsManagement()
        {
           
            IEnumerable<SelectListItem> TournamentsList = DataAccess.Instance.TournamentAction.GetAll().Where(x => x.IsActive == true)
                .Select(x => new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.TornamentName
                }).ToList();
            ViewBag.TournamentsList = TournamentsList;
            
            return View();
        }


        public PartialViewResult GetTeamsPartial(int TournamentId)
        {
            string UserId = User.Identity.GetUserId();
            int TeamId = 0;
            var TeamInformation = DataAccess.Instance.TeamAction.GetAll().Where(x => x.CreateUser == UserId && x.TournamentId == TournamentId).FirstOrDefault();
            if (TeamInformation != null)
            {
                ViewBag.TeamId = TeamInformation.Id;
                ViewBag.TeamName = TeamInformation.TeamName;
                ViewBag.VisibilityClass = "";
                TeamId = TeamInformation.Id;
            }
            else
            {
                ViewBag.TeamId = 0;
                ViewBag.TeamName = string.Empty;
                ViewBag.VisibilityClass = "HideItem";
            }
            
            var TeamIds = DataAccess.Instance.TournamentTeamAction.GetAll().Where(x => x.TournamentId == TournamentId).Select(x => x.TeamId).ToArray();
            IEnumerable<SelectListItem> TeamsList = DataAccess.Instance.TeamAction.GetAll().Where(x => TeamIds.Contains(x.Id) && x.IsVirtualTeam != true)
            .Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.TeamName
            }).ToList();

            ViewBag.TeamsList = TeamsList;
            ViewBag.PlayersList = AvailablePlayersList("Batsman", "0", TeamId, TournamentId);
            ViewBag.TeamPlayersList = GetPlayersList(TeamId);

            return PartialView();
        }

        public ActionResult LeagueManagement()
        {
            IEnumerable<SelectListItem> TournamentsList = DataAccess.Instance.TournamentAction.GetAll().Where(x => x.IsActive == true)
             .Select(x => new SelectListItem
             {
                 Value = x.Id.ToString(),
                 Text = x.TornamentName
             }).ToList();
            ViewBag.TournamentsList = TournamentsList;
            return View();
        }

        public PartialViewResult GetLeaguesPartial()
        {
            IEnumerable<SelectListItem> TournamentsList = DataAccess.Instance.TournamentAction.GetAll().Where(x => x.IsActive == true)
             .Select(x => new SelectListItem
             {
                 Value = x.Id.ToString(),
                 Text = x.TornamentName
             }).ToList();
            ViewBag.TournamentsList = TournamentsList;
            return PartialView();
        }

        [HttpPost]
        public ActionResult SaveTeamInformation(int TournamentId, string TeamTitle, int TeamId)
        {
            try
            {
                string UserId = User.Identity.GetUserId();
                if (DataAccess.Instance.TeamAction.GetAll().Where(x => x.Id != TeamId && x.TeamName == TeamTitle && x.TournamentId == TournamentId).Any())
                {
                    return Json(new { success = false, message = "Team with this title already exists" }, JsonRequestBehavior.AllowGet);
                }

                if (TeamId == 0)
                {
                    Team team = new Team();
                    team.TeamName = TeamTitle;
                    team.IsActive = true;
                    team.CreateUser = UserId;
                    team.CreateDate = DateTime.Now;
                    team.IsVirtualTeam = true;
                    team.TournamentId = TournamentId;
                    DataAccess.Instance.TeamAction.Add(team);
                    TeamId = team.Id;

                    TeamManager TM = new TeamManager();
                    TM.TeamManagerId = UserId;
                    TM.TeamId = TeamId;
                    TM.TeamScore = 0;
                    TM.CreateUser = UserId;
                    TM.CreateDate = DateTime.Now;
                    DataAccess.Instance.TeamManagerAction.Add(TM);
                }
                else
                {
                    Team team = DataAccess.Instance.TeamAction.Get(TeamId);
                    team.TeamName = TeamTitle;
                    team.ModifyDate = DateTime.Now;
                    team.ModifyUser = UserId;
                    DataAccess.Instance.TeamAction.Change(team);
                }

                return Json(new { success = true, TeamId = TeamId }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ee)
            {
                return Json(new { success = false, message = ee }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult AddToTeam(int PlayerId, int TeamId)
        {
            try
            {
                var list = DataAccess.Instance.VirtualPlayerAction.GetAll().Where(x => x.TeamId == TeamId).ToList();
                if (list.Count < 11)
                {
                    VirtualTeamPlayer vtm = new VirtualTeamPlayer();
                    vtm.TeamId = TeamId;
                    vtm.PlayerId = PlayerId;
                    if (list.Count < 11)
                    {
                        DataAccess.Instance.VirtualPlayerAction.Add(vtm);
                        return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = false, message = "11 players required in one team." }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false, message = "You can add maximum 11 players in one team." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ee)
            {
                return Json(new { success = false, message = ee }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult RemoveFromTeam(int PlayerId, int TeamId)
        {
            try
            {
                var vtm = DataAccess.Instance.VirtualPlayerAction.GetAll().Where(x => x.PlayerId == PlayerId && x.TeamId == TeamId).FirstOrDefault();
                if (vtm != null)
                {
                    DataAccess.Instance.VirtualPlayerAction.Delete(vtm.Id);
                }

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ee)
            {
                return Json(new { success = false, message = ee }, JsonRequestBehavior.AllowGet);
            }
        }


        public PartialViewResult GetTeamMembers(int TeamId)
        {

            return PartialView(GetPlayersList(TeamId));
        }

        private List<PlayersDto> GetPlayersList(int TeamId)
        {
            List<PlayersDto> list = DataAccess.Instance.VirtualPlayerAction.GetAll().Where(x => x.TeamId == TeamId).Select(x => new
           PlayersDto
            {
                PlayerName = x.Player.PlayerName,
                PlayerId = x.PlayerId,
                PlayerType = x.Player.PlayerType,
                isCaptain = x.IsCaptain != null ? x.IsCaptain.Value : false,
                IsManOfMatch = x.IsManOfMatch != null ? x.IsManOfMatch.Value : false
            }).ToList();

            return list;
        }

        public PartialViewResult _PlayersSelectionPartial(int TournamentId, string PlayerType, string TeamId, int SelectedTeamId)
        {

            return PartialView(AvailablePlayersList(PlayerType, TeamId, SelectedTeamId, TournamentId));
        }

        private IEnumerable<PlayersDto> AvailablePlayersList(string PlayerType, string TeamId, int SelectedTeamId, int TournamentId)
        {
            int Id = 0;
            if (!string.IsNullOrEmpty(TeamId))
            {
                Id = int.Parse(TeamId);
            }

            int[] LeaguesList = DataAccess.Instance.LeagueTeamAction.GetAll().Where(x => x.TeamId == SelectedTeamId).Select(x => x.LeagueId).ToArray();
            int[] TeamsInSameLeague = DataAccess.Instance.LeagueTeamAction.GetAll().Where(x => LeaguesList.Contains(x.LeagueId)).Select(x => x.TeamId).ToArray();
            int[] PlayersToBeIgnored = DataAccess.Instance.VirtualPlayerAction.GetAll().Where(x => TeamsInSameLeague.Contains(x.TeamId) || x.TeamId == SelectedTeamId).Select(x => x.PlayerId).ToArray();

            if (Id == 0)
            {
                int[] TeamsInSelectedTournament = DataAccess.Instance.TournamentTeamAction.GetAll().Where(x => x.TournamentId == TournamentId).Select(x => x.TeamId).ToArray();

                int[] PlayerIds = DataAccess.Instance.TeamPlayerAction.GetAll().Where(x=> TeamsInSelectedTournament.Contains(x.TeamId)).Select(x => x.PlayerId).ToArray();
                IEnumerable<PlayersDto> PlayersList = DataAccess.Instance.PlayersActions.GetAll().
                 Where(x => x.PlayerType == PlayerType && PlayerIds.Contains(x.Id) && !PlayersToBeIgnored.Contains(x.Id))
                 .Select(x => new PlayersDto
                 {
                     PlayerId = x.Id,
                     PlayerValue = x.Value,
                     PlayerName = x.PlayerName
                 }).ToList();
                return PlayersList;
            }
            else
            {
                int[] PlayerIds = DataAccess.Instance.TeamPlayerAction.GetAll().Where(x => x.TeamId == Id).Select(x => x.PlayerId).ToArray();
                IEnumerable<PlayersDto> PlayersList = DataAccess.Instance.PlayersActions.GetAll().
                 Where(x => x.PlayerType == PlayerType && PlayerIds.Contains(x.Id) && !PlayersToBeIgnored.Contains(x.Id))
                 .Select(x => new PlayersDto
                 {
                     PlayerId = x.Id,
                     PlayerValue = x.Value,
                     PlayerName = x.PlayerName
                 }).ToList();
                return PlayersList;
            }
        }

        [HttpPost]
        public ActionResult SetAsCaptain(int PlayerId, int TeamId)
        {
            try
            {

                var vtm = DataAccess.Instance.VirtualPlayerAction.GetAll().Where(x => x.IsCaptain == true && x.TeamId == TeamId).FirstOrDefault();
                if (vtm != null)
                {
                    vtm.IsCaptain = false;
                    DataAccess.Instance.VirtualPlayerAction.Change(vtm);
                }

                var vtm1 = DataAccess.Instance.VirtualPlayerAction.GetAll().Where(x => x.PlayerId == PlayerId && x.TeamId == TeamId).FirstOrDefault();
                if (vtm1 != null)
                {
                    vtm1.IsCaptain = true;
                    DataAccess.Instance.VirtualPlayerAction.Change(vtm1);
                }

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ee)
            {
                return Json(new { success = false, message = ee }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SetAsManOfMatch(int PlayerId, int TeamId)
        {
            try
            {

                var vtm = DataAccess.Instance.VirtualPlayerAction.GetAll().Where(x => x.IsManOfMatch == true && x.TeamId == TeamId).FirstOrDefault();
                if (vtm != null)
                {
                    vtm.IsManOfMatch = false;
                    DataAccess.Instance.VirtualPlayerAction.Change(vtm);
                }

                var vtm1 = DataAccess.Instance.VirtualPlayerAction.GetAll().Where(x => x.PlayerId == PlayerId && x.TeamId == TeamId).FirstOrDefault();
                if (vtm1 != null)
                {
                    vtm1.IsManOfMatch = true;
                    DataAccess.Instance.VirtualPlayerAction.Change(vtm1);
                }

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ee)
            {
                return Json(new { success = false, message = ee }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CheckTeam()
        {
            string UserId = User.Identity.GetUserId();
            bool HasTeam = DataAccess.Instance.TeamAction.GetAll().Where(x => x.IsVirtualTeam == true && x.CreateUser == UserId).Any();

            return Json(new { success = HasTeam }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckTeamHasPlayersandMOM()
        {
            string UserId = User.Identity.GetUserId();
            var team = DataAccess.Instance.TeamAction.GetAll().Where(x => x.IsVirtualTeam == true && x.CreateUser == UserId).First();
            var teamPlayers = DataAccess.Instance.VirtualPlayerAction.GetAll().Where(x => x.TeamId == team.Id).Count();
            if (teamPlayers < 11)
            {
                return Json(new { success = false, message = "You team does not have 11 players. Please add them first" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                bool HasManOfMatch = DataAccess.Instance.VirtualPlayerAction.GetAll().Where(x => x.IsManOfMatch == true && x.TeamId == team.Id).Any();
                if (HasManOfMatch)
                {
                    return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = "You team does not have Man of Match selected. Please select one first." }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult GetLeagueTitle(int LeagueId)
        {
            string LeagueTitle = string.Empty;
            string Password = string.Empty;
            var LeagueInfo = DataAccess.Instance.LeagueAction.Get(LeagueId);
            Password = LeagueInfo.LeaguePassword;
            LeagueTitle = LeagueInfo.LeagueName;

            return Json(new { LeagueTitle = LeagueTitle, Password = Password }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult CreateLeague(int TournamentId, int LeagueId, string LeagueTitle, string Password)
        {
            try
            {
                bool ValidateName = DataAccess.Instance.LeagueAction.GetAll().Where(x => x.Id != LeagueId && x.LeagueName == LeagueTitle && x.TournamentId == TournamentId).Any();
                if (ValidateName)
                {
                    return Json(new { success = false, message = "League with this title already exists" }, JsonRequestBehavior.AllowGet);
                }

                string UserId = User.Identity.GetUserId();

                if (LeagueId == 0)
                {
                    League league = new League();
                    league.LeagueCommisionerId = UserId;
                    league.LeagueName = LeagueTitle;
                    league.LeaguePassword = Password;
                    league.CreateDate = DateTime.Now;
                    league.CreateUser = UserId;
                    league.TournamentId = TournamentId;
                    DataAccess.Instance.LeagueAction.Add(league);
                }
                else
                {
                    League league = DataAccess.Instance.LeagueAction.Get(LeagueId);
                    league.LeagueName = LeagueTitle;
                    league.LeaguePassword = Password;
                    league.ModifyDate = DateTime.Now;
                    league.ModifyUser = UserId;
                    DataAccess.Instance.LeagueAction.Change(league);
                }

                return Json(new { success = true, message = "League Information Saved Successfully" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ee)
            {
                return Json(new
                {
                    success = false,
                    message = ee
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult GetLeaguesGrid(int TournamentId)
        {
            string UserId = User.Identity.GetUserId();
            List<LeaguesDTO> leagues = DataAccess.Instance.LeagueAction.GetAll().Where(x => x.LeagueCommisionerId == UserId && x.TournamentId == TournamentId).AsEnumerable().Select(x => new LeaguesDTO
            {
                LeagueId = x.Id,
                CreatedDate = x.CreateDate.ToShortDateString(),
                LeagueTitle = x.LeagueName,
                TotalTeams = DataAccess.Instance.LeagueTeamAction.GetAll().Where(y => y.LeagueId == x.Id).Count(),
                Password = x.LeaguePassword
            }).ToList();
            return PartialView(leagues);
        }

        public PartialViewResult GetEnrolledLeaguesGrid(int TournamentId)
        {
            string UserId = User.Identity.GetUserId();
            var TeamInfo = DataAccess.Instance.TeamAction.GetAll().Where(x => x.CreateUser == UserId && x.IsVirtualTeam == true).FirstOrDefault();
            List<LeaguesDTO> leagues = new List<LeaguesDTO>();

            if (TeamInfo != null)
            {
                int[] Leagues = DataAccess.Instance.LeagueTeamAction.GetAll().Where(x => x.TeamId == TeamInfo.Id).Select(x => x.LeagueId).ToArray();
                leagues = DataAccess.Instance.LeagueAction.GetAll().Where(x => Leagues.Contains(x.Id) &&  x.TournamentId == TournamentId).AsEnumerable().Select(x => new LeaguesDTO
                {
                    LeagueId = x.Id,
                    LeagueTitle = x.LeagueName,
                    TotalTeams = DataAccess.Instance.LeagueTeamAction.GetAll().Where(y => y.LeagueId == x.Id).Count(),
                }).ToList();
            }

            return PartialView(leagues);
        }

        public PartialViewResult GetOverallLeagues(int TournamentId)
        {
            string UserId = User.Identity.GetUserId();
            var TeamInfo = DataAccess.Instance.TeamAction.GetAll().Where(x => x.CreateUser == UserId && x.IsVirtualTeam == true ).FirstOrDefault();
            List<LeaguesDTO> leagues = new List<LeaguesDTO>();

            if (TeamInfo != null)
            {
                int[] Leagues = DataAccess.Instance.LeagueTeamAction.GetAll().Where(x => x.TeamId == TeamInfo.Id).Select(x => x.LeagueId).ToArray();
                leagues = DataAccess.Instance.LeagueAction.GetAll().Where(x => !Leagues.Contains(x.Id) && x.LeagueCommisionerId != UserId && x.TournamentId == TournamentId).AsEnumerable().Select(x => new LeaguesDTO
                {
                    LeagueId = x.Id,
                    LeagueTitle = x.LeagueName,
                    TotalTeams = DataAccess.Instance.LeagueTeamAction.GetAll().Where(y => y.LeagueId == x.Id).Count(),
                }).ToList();
            }
            else
            {
                leagues = DataAccess.Instance.LeagueAction.GetAll().Where(x => x.LeagueCommisionerId != UserId && x.TournamentId == TournamentId).AsEnumerable().Select(x => new LeaguesDTO
                {
                    LeagueId = x.Id,
                    LeagueTitle = x.LeagueName,
                    TotalTeams = DataAccess.Instance.LeagueTeamAction.GetAll().Where(y => y.LeagueId == x.Id).Count(),
                }).OrderBy(x => x.TotalTeams).ToList();
            }

            return PartialView(leagues);
        }

        [HttpPost]
        public ActionResult JoinLeague(string Password, int LeagueId)
        {
            string UserId = User.Identity.GetUserId();
            var Team = DataAccess.Instance.TeamAction.GetAll().Where(x => x.IsVirtualTeam == true && x.CreateUser == UserId).FirstOrDefault();

            if (Team == null)
            {
                return Json(new { success = false, message = "Please create a team first to join any league" }, JsonRequestBehavior.AllowGet);
            }


            var info = DataAccess.Instance.LeagueAction.GetAll().Where(x => x.Id == LeagueId && x.LeaguePassword == Password).FirstOrDefault();
            if (info != null)
            {
                LeagueTeam lg = new LeagueTeam();
                lg.LeagueId = LeagueId;
                lg.TeamId = Team.Id;
                DataAccess.Instance.LeagueTeamAction.Add(lg);
                return Json(new { success = true, message = "League joined!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, message = "Invalid password" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult JoinOwnLeague(int LeagueId)
        {
            string UserId = User.Identity.GetUserId();
            var Team = DataAccess.Instance.TeamAction.GetAll().Where(x => x.IsVirtualTeam == true && x.CreateUser == UserId).FirstOrDefault();

            if (Team == null)
            {
                return Json(new { success = false, message = "Please create a team first to join any league" }, JsonRequestBehavior.AllowGet);
            }


            var info = DataAccess.Instance.LeagueAction.GetAll().Where(x => x.Id == LeagueId && x.CreateUser == UserId).FirstOrDefault();
            if (info != null)
            {
                var LeagueTeam = DataAccess.Instance.LeagueTeamAction.GetAll().Where(x => x.LeagueId == LeagueId && x.TeamId == Team.Id).FirstOrDefault();
                if (LeagueTeam == null)
                {
                    LeagueTeam lg = new LeagueTeam();
                    lg.LeagueId = LeagueId;
                    lg.TeamId = Team.Id;
                    DataAccess.Instance.LeagueTeamAction.Add(lg);
                    return Json(new { success = true, message = "League joined!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = "You have already joined this league" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, message = "Oops! something went wrong while joining this league." }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SendInvites(string Emails, int LeagueId)
        {
            try
            {
                var LeagueInfo = DataAccess.Instance.LeagueAction.Get(LeagueId);
                List<string> emails = Emails.Split(',').ToList<string>();

                foreach (var email in emails)
                {
                    SendMail(email, LeagueInfo.LeaguePassword, LeagueInfo.LeagueName);
                }

                return Json(new { success = true, message = "Invitations sent" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ee)
            {
                return Json(new { success = false, message = ee }, JsonRequestBehavior.AllowGet);
            }
        }

        public void SendMail(string Email, string Password, string LeagueTitle)
        {
            string html = string.Empty;

            html = "<div>League Invitation is Sent To You </div>";
            html += "<div>League Title: " + LeagueTitle + "</div>";
            html += "League Pasword: " + Password;

            var fromAddress = new MailAddress("dnsenterprises7039@gmail.com", "Cricket Fantasy - League Invitation");
            var toAddress = new MailAddress(Email);
            const string fromPassword = "dnsenterprises";
            const string subject = "League Invitation";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword),

            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                IsBodyHtml = true,
                Subject = subject,
                Body = html
            })
            {
                smtp.Send(message);
            }
        }


        public ActionResult LeagueTeams(int LeagueId)
        {
            List<TeamDTO> teamsList = DataAccess.Instance.LeagueTeamAction.GetAll().Where(x => x.LeagueId == LeagueId).AsEnumerable().Select(x => new TeamDTO
            {
                TeamName = x.Team.TeamName,
                TeamId = x.TeamId,
                TotalPoints = GetPlayerPoints(x.TeamId)
            }).OrderByDescending(x => x.TotalPoints).ToList();
            return View(teamsList);
        }

        private int? GetPlayerPoints(int teamId)
        {
            int[] ss = DataAccess.Instance.VirtualPlayerAction.GetAll().Where(x => x.TeamId == teamId).Select(x => x.PlayerId).ToArray();
            return DataAccess.Instance.PlayersActions.GetAll().Where(x => ss.Contains(x.Id)).Sum(x => x.TotalPoints);
        }

        public ActionResult LeagueTeamPlayers(int TeamId)
        {

            List<PlayersDto> PlayersList = DataAccess.Instance.VirtualPlayerAction.GetAll().Where(x => x.TeamId == TeamId).AsEnumerable().Select(x => new PlayersDto
            {
                PlayerName = x.Player.PlayerName,
                Points = x.Player.TotalPoints != null ? x.Player.TotalPoints.Value : 0
            }).OrderByDescending(x => x.Points).ToList();
            return View(PlayersList);
        }

        public ActionResult Profile()
        {
            string UserId = User.Identity.GetUserId();

            AspNetUser aspNetUser = DataAccess.Instance.UsersAction.GetAll().Where(x => x.Id == UserId).FirstOrDefault();

            return View(aspNetUser);
        }

    }
}