﻿$(document).ready(function () {
    $(document).ajaxStart(showLoadingScreen(true)).ajaxStop(showLoadingScreen(false));
});


function showLoadingScreen(enabled) {
    return enabled ? $.blockUI({ message: '<div class="blockUI"><div class="spinner arrow"><div class="arrow1"></div></div></div>' }) : $.unblockUI();
}
$(document).ajaxStart(function () {
    showLoadingScreen(true);
}).ajaxStop(function () {
    showLoadingScreen(false);
});

function GetUsers() {
    $.ajax({
        type: "GET",
        url: '/Home/GetUsers',
        success: function (result) {
            $("#MainUsersDiv").html(result);
        }
    });
}

function GetUsersPost(p_strName) {
    $.ajax({
        type: "POST",
        url: '/Home/GetUsers',
        data: { p_strName: p_strName },
        success: function (result) {
            $("#MainUsersDiv").html('');
            $("#MainUsersDiv").html(result);
        }
    });
}

function InitializeTable(Table) {
    var UsersTable = $("#" + Table).dataTable({
        "autoWidth": false,
        destroy: true,
        paging: true,
        sort: true,
        searching: true,
        "orderCellsTop": true,

    });
    if ($(".dataTables_empty").length) {
        $(".dataTables_paginate.paging_simple_numbers").hide();
    }
    if ($(".dataTables_empty").length) {
        $(".dataTables_length").hide();
    }

    $('#' + Table + ' tbody').on('mouseenter', 'td', function () {
        if ($(this).find(".fa-pencil").length == 0) {
            this.setAttribute('title', $(this).text());
        }
    });
};


function CreateUserManually() {
    $.ajax({
        type: "POST",
        url: '/Account/CreateUserManually',
        data: { p_strEmail: $('#Email').val(), p_strPassword: $('#Password').val(), p_strRole: $('#UserRoleId option:selected').text() },
        success: function (result) {

            if (result.success) {
                $.toaster({ priority: 'success', message: result.message });
                GetUsers();
                $("#UsersModal").modal("hide");
            }
            else {
                $.toaster({ priority: 'danger', message: result.message });
            }
        }
    });
}

function UpdateUserByAdminWithoutPassword() {
    $.ajax({
        type: "POST",
        url: '/Account/CreateUserManually',
        data: { p_strEmail: $('#Email').val(), p_strPassword: $('#Password').val(), p_strRole: $('#UserRoleId option:selected').text() },
        success: function (result) {

            if (result.success) {
                $.toaster({ priority: 'success', message: result.message });
                GetUsers();
                $("#UsersModal").modal("hide");
            }
            else {
                $.toaster({ priority: 'danger', message: result.message });
            }
        }
    });
}

function GetUsersModel(UserId) {
    $.ajax({
        type: "GET",
        url: '/Home/CreateUser',
        data: { UserId: UserId },
        success: function (result) {
            if (UserId == "0") {
                $("#AccountsTitleDiv").text("Create User");
                $("#SubmitAccountForm").text("Create User");
                $('#SubmitAccountForm').attr('onclick', 'CreateUserManually()');
            }
            else {
                $("#AccountsTitleDiv").text("Update User");
                $("#SubmitAccountForm").text("Update User");
                $('#SubmitAccountForm').attr('onclick', '');
            }
            $("#CreateUserDiv").html(result);
            $("#UsersModal").modal("show");
        },
    });
}

function CreateUserSuccess(response) {
    $("#CreateUserDiv").html('');
    $.toaster({ priority: 'success', message: 'User information saved successfully' });
    GetUsers();
    $("#UsersModal").modal("hide");
}

//team grid functions 

function GetTeamModel(TeamId) {
    $.ajax({
        type: "GET",
        url: '/Team/CreateTeam',
        data: { TeamId: TeamId },
        success: function (result) {
            if (TeamId == '0') {
                $("#TeamsTitleDiv").text("Manage Team");
                $("#SubmitTeamsBtn").text("Save Team");
            }
            else {
                $("#TeamsTitleDiv").text("Manage Team");
                $("#SubmitTeamsBtn").text("Save Team");
            }
            $("#CreateTeamDiv").html(result);
            $("#TeamModal").modal("show");
        },
    });
}

$(document).on("click", "#SubmitTeamsBtn", function () {
    $("#CreateTeamsBtn").trigger("click");
});


function GetTeamData() {
    $.ajax({
        type: "GET",
        url: '/Team/GetTeamData',
        success: function (result) {
            $("#MainTeamDiv").html(result);
        },
    });
}

function GetVirutalTeamData() {
    $.ajax({
        type: "GET",
        url: '/Team/GetVirtualTeamData',
        success: function (result) {
            $("#MainVirutalTeamDiv").html(result);
        }
    });
}

function CreateTeamSuccess(response) {
    $("#TeamId").val(response.TeamId);
    $.toaster({ priority: 'success', message: 'Team information saved successfully' });
}


function GetPlayerModel(PlayerId) {

    if ($("#TeamId").val() == '0') {
        $.toaster({ priority: 'danger', message: 'Please add team first' });
        return;
    }

    $.ajax({
        type: "GET",
        url: '/Players/CreatePlayer',
        data: { PlayerId: PlayerId, TeamId: $("#TeamId").val() },
        success: function (result) {
            if (PlayerId == "0") {
                $("#PlayerModelTitle").text("Create Player");
                $("#SubmitPlayerForm").text("Create Player");
            }
            else {
                $("#PlayerModelTitle").text("Update Player");
                $("#SubmitPlayerForm").text("Update Player");
            }
            $("#CreatePlayerDiv").html(result);
            $("#PlayerModal").modal("show");
        },
    });
}


$(document).on("click", "#SubmitPlayerForm", function () {
    $("#CreatePlayerBtn").trigger("click");
});

function GetPlayersData() {
    $.ajax({
        type: "GET",
        data: { TeamId: $('#TeamId').val() },
        url: '/Players/GetPlayersData',
        success: function (result) {
            $("#PlayersContainerDiv").html(result);
        },
    });
}

function CreatePlayerSuccess(response) {
    GetPlayersData();
    $.toaster({ priority: 'success', message: 'Player information saved successfully' });
    CloseModal('PlayerModal')
}

function CloseModal(ModalId) {
    $("#" + ModalId).modal('hide');
}


function GetTournamentsList() {
    $.ajax({
        type: "GET",
        url: "/Home/GetTournamentsList",
        success: function (response) {

            $("#TournamentsDropdown").empty();
            $("#TournamentsDropdown").append($("<option> </option>").val(0).html("Select Tournament"));
            $.each(response.data, function () {
                $("#TournamentsDropdown").append($("<option> </option>").val(this['id']).html(this['value']));
            });

            $("#TournamentsDropdown").select2();
        },
    });
}

function GetTournamentDetails(Id) {
    $("#DeleteTournamentButton").show();
    if (Id == "0") {
        $("#DeleteTournamentButton").hide();
    }

    $.ajax({
        type: "GET",
        url: "/Home/_GetTournamentDetails",
        data: { TournamentId: Id },
        success: function (response) {
            $("#TournamentDetailsContainer").html(response);
        },
    });
}

function DeleteTournament(TournamentId) {

    swal({
        title: "Delete Tournament",
        text: "Are you sure! you want delete this Tournament?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((Confirm) => {
            if (Confirm) {
                $.ajax({
                    type: "post",
                    url: '/Home/DeleteTournament',
                    data: { TournamentId: TournamentId },
                    success: function (result) {
                        if (result.success) {
                            GetTournamentsList();
                            $("#TournamentDetailsContainer").html('');
                            $("#DeleteTournamentButton").hide();
                            $.toaster({ priority: 'success', message: "Tournament removed successfully" });
                        }
                        else {
                            $.toaster({ priority: 'danger', message: result.message });
                        }
                    }
                });
            }
        });
}

function SaveTournamentChanges(Id) {
   
    if ($("#TournamentName").val().trim() == "" || $('#TournamentType option:selected').text().trim() == "Select Tournament Type" || $("#Venue").val().trim() == "" || $("#StartDate").val().trim() == "" || $("#EndDate").val().trim() == "" || $("#TeamBudget").val().trim() == "" || $("#NoOfChangesAllowed").val().trim() == "" || $("#NumberofTeams").val().trim() == "") {
        $.toaster({ priority: 'danger', message: 'Please enter all tournament basic information' });
        return;
    }
    $.ajax({
        type: "GET",
        url: "/Home/SaveTournament",
        data: { TournamentId: $("#TournamentId").val(), TournamentName: $("#TournamentName").val(),  TournamentType: $('#TournamentType option:selected').text().trim(), Venue: $("#Venue").val(), StartDate: $("#StartDate").val(), EndDate: $("#EndDate").val(), TeamBudget: $("#TeamBudget").val(), NoOfChangesAllowed: $("#NoOfChangesAllowed").val(), NumberofTeams: $("#NumberofTeams").val(), },
        success: function (response) {
            if (response.success) {
                $("#TournamentId").val(response.TournamentId);
                $.toaster({ priority: 'success', message: response.message });
                GetTournamentsList();
            }
            else {
                $.toaster({ priority: 'danger', message: response.message });
            }
        },
    });
}

function GetTeamsToSelect() {
    $.ajax({
        type: "GET",
        url: "/Home/GetTeamsNotInTournament",
        data: { tournamentId: $("#TournamentId").val() },
        success: function (response) {
            $("#TeamSelectionDropdown").empty();
            $("#TeamSelectionDropdown").append($("<option> </option>").val(0).html("Select Team"));
            $.each(response.data, function () {
                $("#TeamSelectionDropdown").append($("<option> </option>").val(this['TeamId']).html(this['TeamName']));
            });

            $("#TeamSelectionDropdown").select2();
        },
    });
}


function GetTeamsInTournament() {
    $.ajax({
        type: "GET",
        url: "/Home/GetListofTeamsInTournament",
        data: { TournamentId: $("#TournamentId").val() },
        success: function (response) {
            $("#TournamentTeamsContainer").html(response);
        },
    });
}

function AddTeamtoTournament() {

    if ($("#TeamSelectionDropdown").val() == 0) {
        $.toaster({ priority: 'danger', message: "Please select team to add" });
        return;
    }

    if ($("#TournamentId").val() == 0) {
        $.toaster({ priority: 'danger', message: "Please save tournament information first!" });
        return;
    }

    $.ajax({
        type: "GET",
        url: "/Home/AddTeamToTournament",
        data: { TeamId: $("#TeamSelectionDropdown").val(), TournamentId: $("#TournamentId").val() },
        success: function (response) {
            if (response.success) {
                $.toaster({ priority: 'success', message: response.message });
                GetTeamsToSelect();
                GetTeamsInTournament();
                GetTeamsWithNoSchedule();
            }
            else {
                $.toaster({ priority: 'danger', message: response.message });
            }
        },
    });
}


function GetTeamsWithNoSchedule() {
    $.ajax({
        type: "GET",
        url: "/Home/GetTeamsWithNoSchedule",
        data: { TournamentId: $("#TournamentId").val() },
        success: function (response) {
            $("#TeamADropdown").empty();
            $("#TeamADropdown").append($("<option> </option>").val(0).html("Select Team"));

            $("#TeamBDropdown").empty();
            $("#TeamBDropdown").append($("<option> </option>").val(0).html("Select Team"));
            $.each(response.data, function () {
                $("#TeamADropdown").append($("<option> </option>").val(this['TeamId']).html(this['TeamName']));
                $("#TeamBDropdown").append($("<option> </option>").val(this['TeamId']).html(this['TeamName']));
            });

            $("#TeamADropdown").select2();
            $("#TeamBDropdown").select2();
        },

    });
}

function GetTournamentSchedule() {
    $.ajax({
        type: "GET",
        url: "/Home/_GetTournamentSchedules",
        data: { TournamentId: $("#TournamentId").val() },
        success: function (response) {
            $("#TournamentSchedulesContainer").html(response);
        },
    });
}

function AddTeamsSchedule() {

    if ($("#TeamADropdown").val() == $("#TeamBDropdown").val()) {
        $.toaster({ priority: 'danger', message: "Please select different teams" });
        return;
    }

    if ($("#TeamADropdown").val() == 0 || $("#TeamBDropdown").val() == 0) {
        $.toaster({ priority: 'danger', message: "Please select teams to add" });
        return;
    }

    if ($("#MatchDate").val() == "") {
        $.toaster({ priority: 'danger', message: "Please select match date!" });
        return;
    }

    $.ajax({
        type: "GET",
        url: "/Home/AddTeamsSchedule",
        data: { TeamAId: $("#TeamADropdown").val(), TeamBId: $("#TeamBDropdown").val(), MatchSchedule: $("#MatchDate").val(), TournamentId: $("#TournamentId").val() },
        success: function (response) {
            if (response.success) {
                $.toaster({ priority: 'success', message: response.message });
                GetTeamsWithNoSchedule();
                GetTournamentSchedule();
            }
            else {
                $.toaster({ priority: 'danger', message: response.message });
            }
        },
    });
}

function RemoveTeamFromTournament(TeamId) {
    $.ajax({
        type: "Post",
        url: '/Home/RemoveTeamFromTournament',
        data: { TeamId: TeamId, TournamentId: $("#TournamentId").val() },
        success: function (result) {
            $("#TournamentsDropdown").trigger('change');
            $.toaster({ priority: 'success', message: "Team removed from tournament" });
        }
    });
}

function CancelMatch(ScheduleId) {
    $.ajax({
        type: "Post",
        url: '/Home/CancelMatch',
        data: { ScheduleId: ScheduleId },
        success: function (result) {
            $("#TournamentsDropdown").trigger('change');
            $.toaster({ priority: 'success', message: "Match cancelled" });
        }
    });
}

function GetPlayerStatics(PlayerId) {

}

$(document).on("click", "#ImageUserDiv", function () {
    $("#UserImage").trigger("click");
});

function readURLUser(input) {
    if (input.files) {

        $("#ImageUserDiv").html('');

        var anyWindow = window.URL || window.webkitURL;
        for (var i = 0; i < input.files.length; i++) {
            var objectUrl = anyWindow.createObjectURL(input.files[i]);
            $('#ImageUserDiv').append('<img src="' + objectUrl + '" />');
            window.URL.revokeObjectURL(input.files[i]);
        }
    }
}

$(document).on("click", "#ImageDiv", function () {
    $("#PlayerImage").trigger("click");
});

function readURL(input) {
    if (input.files) {

        $("#ImageDiv").html('');

        var anyWindow = window.URL || window.webkitURL;
        for (var i = 0; i < input.files.length; i++) {
            var objectUrl = anyWindow.createObjectURL(input.files[i]);
            $('#ImageDiv').append('<img src="' + objectUrl + '" />');
            window.URL.revokeObjectURL(input.files[i]);
        }
    }
}

function ValidatePlayer() {
    if ($("#PlayerName").val().trim() == "" || $("#PlayerValue").val().trim() == "" || $("#PlayerType").val().trim() == "") {
        $.toaster({ priority: 'danger', message: "Please enter player name, player type & player value" });
        return;
    }

    $.ajax({
        type: "Post",
        url: '/Home/ValidatePlayerName',
        data: { PlayerId: $("#PlayerId").val(), PlayerName: $("#PlayerName").val() },
        success: function (result) {
            if (result.success) {
                SavePlayerChanges();
            }
            else {
                $.toaster({ priority: 'danger', message: "Player with same name already exists" });
            }
        }
    });
}

function SavePlayerChanges() {
    var PlayerId = $("#PlayerId").val();
    var PlayerName = $("#PlayerName").val();
    var PlayerValue = $("#PlayerValue").val();
    var PlayerType = $("#PlayerType").val();
    var Statics = [];

    $(".TournamentSectionForPlayer").each(function () {

        Statics.push({
            MatchType: $(this).attr('id'),
            TotalMatches: $(this).find('.TotalMatches').val(),
            InningsBatted: $(this).find('.InningsBatted').val(),
            NotOuts: $(this).find('.NotOuts').val(),
            RunsScored: $(this).find('.RunsScored').val(),
            HighestScore: $(this).find('.HighestScore').val(),
            Average: $(this).find('.Average').val(),
            BallsFaced: $(this).find('.BallsFaced').val(),
            StrikeRate: $(this).find('.StrikeRate').val(),
            Hundred: $(this).find('.Hundred').val(),
            Fifty: $(this).find('.Fifty').val(),
            Fours: $(this).find('.Fours').val(),
            Sixes: $(this).find('.Sixes').val(),
            CatchesTaken: $(this).find('.CatchesTaken').val(),
            BallsBowled: $(this).find('.BallsBowled').val(),
            RunsGiven: $(this).find('.RunsGiven').val(),
            WicketsTaken: $(this).find('.WicketsTaken').val(),
            Economy: $(this).find('.Economy').val(),
            FourWicketsInMatch: $(this).find('.FourWicketsInMatch').val(),
            FiveWicketsInMatch: $(this).find('.FiveWicketsInMatch').val(),
            TenWicketsInMatch: $(this).find('.TenWicketsInMatch').val(),
        });
    });

    Statics = JSON.stringify(Statics);

    var formData = new FormData();
    formData.append("PlayerId", PlayerId);
    formData.append("PlayerName", PlayerName);
    formData.append("PlayerValue", PlayerValue);
    formData.append("PlayerType", PlayerType);
    formData.append("Statics", Statics);

    var $file = $("#PlayerImage").get(0).files;
    if ($file.length > 0) {
        formData.append('file1', $file[0]);
    }

    $.ajax({
        type: 'POST',
        url: "/Home/SavePlayerDetails",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
            if (response.success) {
                $.toaster({ priority: 'success', message: response.message });
            }
            else {
                $.toaster({ priority: 'danger', message: response.message });
            }
        }
    });
}

function AddToTeam(PlayerId, $this) {

    if ($("#TeamId").val() == '0') {
        $.toaster({ priority: 'danger', message: 'Please save team first' });
        return;
    }
    $.ajax({
        type: "Post",
        url: '/Home/AddToTeam',
        data: { PlayerId: PlayerId, TeamId: $("#TeamId").val() },
        success: function (result) {
            $($this).parent('td').html('<span class="btn btn-danger btn-sm" onclick="RemoveFromTeam(' + PlayerId + ', this)">Remove From Team</span>');
            $.toaster({ priority: 'success', message: "Player added to team successfully" });
        }
    });
}

function RemoveFromTeam(PlayerId, $this) {
    $.ajax({
        type: "Post",
        url: '/Home/RemoveFromTeam',
        data: { PlayerId: PlayerId, TeamId: $("#TeamId").val() },
        success: function (result) {
            $($this).parent('td').html('<span class="btn btn-primary btn-sm" onclick="AddToTeam(' + PlayerId + ', this)">Add to Team</span>');
            $.toaster({ priority: 'success', message: "Player removed from team" });
        }
    });
}

function RemoveRow($this) {
    $($this).parents('tr').remove();
}

function SubmitMatchResults() {
    var DataIsComplete = true;
    if ($("#TeamATotalScore").val() == "" || $("#TeamAOversPlayed").val() == "" || $("#TeamAPlayersOut").val() == "" || $("#TeamBTotalScore").val() == "" || $("#TeamBOversPlayed").val() == "" || $("#TeamBPlayersOut").val() == "") {
        $.toaster({ priority: 'danger', message: "please enter teams details" });
        return;
    }


    var TeamAInningDetails = [];
    var TeamBInningDetails = [];
    var TeamABowlingDetails = [];
    var TeamBBowlingDetails = [];
    $("#TeamAInningsTable tr").each(function (i) {

        $(this).find('input').each(function () {
            if ($(this).val() == "") {
                DataIsComplete = false;
            }
        });

        if (i > 0) {
            TeamAInningDetails.push({
                PlayerId: $(this).find('.PlayerIdHidden').val(),
                OutStatus: $(this).find('.OutNotOutDropdown').val(),
                OutType: $(this).find('.OutTypeDropdown').val(),
                OutByPlayerId: $(this).find('.OutByPlayer').val(),
                Runs: $(this).find('.RunsTxtbx').val(),
                Balls: $(this).find('.BallsTxtbx').val(),
                Minutes: $(this).find('.MinutesTxtbx').val(),
                Fours: $(this).find('.FoursTxtBx').val(),
                Sixes: $(this).find('.SixesTxtBx').val(),
                StrikeRate: $(this).find('.StrikeRateTxtBx').val(),
            });
        }
    });

    $("#TeamBBowlingTable tr").each(function (i) {

        $(this).find('input').each(function () {
            if ($(this).val() == "") {
                DataIsComplete = false;
            }
        });

        if (i > 0) {
            TeamBBowlingDetails.push({
                PlayerId: $(this).find('.PlayerIdHidden').val(),
                Overs: $(this).find('.OversTxtBx').val(),
                Maiden: $(this).find('.MaidenTxtbx').val(),
                Runs: $(this).find('.RunsTxtBx').val(),
                Wickets: $(this).find('.WicketsTxtBx').val(),
                Economy: $(this).find('.EcomonyTxtBx').val(),
                Fours: $(this).find('.FoursGivenTxtBx').val(),
                Sixes: $(this).find('.SixesGivenTxtbx').val(),
                Wides: $(this).find('.WideBallsTxtbx').val(),
                NoBalls: $(this).find('.NoBallsTxtbx').val(),
            });
        }
    });

    $("#TeamBInningsTable tr").each(function (i) {

        $(this).find('input').each(function () {
            if ($(this).val() == "") {
                DataIsComplete = false;
            }
        });

        if (i > 0) {
            TeamBInningDetails.push({
                PlayerId: $(this).find('.PlayerIdHidden').val(),
                OutStatus: $(this).find('.OutNotOutDropdown').val(),
                OutType: $(this).find('.OutTypeDropdown').val(),
                OutByPlayerId: $(this).find('.OutByPlayer').val(),
                Runs: $(this).find('.RunsTxtbx').val(),
                Balls: $(this).find('.BallsTxtbx').val(),
                Minutes: $(this).find('.MinutesTxtbx').val(),
                Fours: $(this).find('.FoursTxtBx').val(),
                Sixes: $(this).find('.SixesTxtBx').val(),
                StrikeRate: $(this).find('.StrikeRateTxtBx').val(),
            });
        }
    });

    $("#TeamABowlingTable tr").each(function (i) {

        $(this).find('input').each(function () {
            if ($(this).val() == "") {
                DataIsComplete = false;
            }
        });


        if (i > 0) {
            TeamABowlingDetails.push({
                PlayerId: $(this).find('.PlayerIdHidden').val(),
                Overs: $(this).find('.OversTxtBx').val(),
                Maiden: $(this).find('.MaidenTxtbx').val(),
                Runs: $(this).find('.RunsTxtBx').val(),
                Wickets: $(this).find('.WicketsTxtBx').val(),
                Economy: $(this).find('.EcomonyTxtBx').val(),
                Fours: $(this).find('.FoursGivenTxtBx').val(),
                Sixes: $(this).find('.SixesGivenTxtbx').val(),
                Wides: $(this).find('.WideBallsTxtbx').val(),
                NoBalls: $(this).find('.NoBallsTxtbx').val(),
            });
        }
    });

    var data = {
        MatchId: $("#ScheduleId").val(),
        TeamAScore: $("#TeamATotalScore").val(),
        TeamAOversPlayed: $("#TeamAOversPlayed").val(),
        TeamAPlayersOut: $("#TeamAPlayersOut").val(),
        TeamBScore: $("#TeamBTotalScore").val(),
        TeamBOversPlayed: $("#TeamBOversPlayed").val(),
        TeamBPlayersOut: $("#TeamBPlayersOut").val(),
        Winner: $("#WinningTeamDropDown").val(),
        WinCondition: $("#WonByDropdown").val(),

        TeamAExtras: $("#TeamAExtras").val(),
        TeamADidntBat: $("#TeamADidntBat").val(),
        TeamAFallofWickets: $("#TeamAFallofWickets").val(),

        TeamBExtras: $("#TeamBExtras").val(),
        TeamBDidntBat: $("#TeamBDidntBat").val(),
        TeamBFallofWickets: $("#TeamBFallofWickets").val(),

        TeamAInningDetails: JSON.stringify(TeamAInningDetails),
        TeamBInningDetails: JSON.stringify(TeamBInningDetails),
        TeamABowlingDetails: JSON.stringify(TeamABowlingDetails),
        TeamBBowlingDetails: JSON.stringify(TeamBBowlingDetails),
    };

    if (DataIsComplete) {
        $.ajax({
            type: "Post",
            url: '/Home/SubmitMatchResults',
            data: data,
            success: function (result) {
                $.toaster({ priority: 'success', message: "Match result submitted successfully" });
                setTimeout(function () {
                    window.location.href = "/Home/Tournaments";
                }, 1000);
            }
        });
    }
    else {
        $.toaster({ priority: 'danger', message: "Please submit all players information. Or remove unnecessary rows" });
    }
}

function ShowPlayerProfile(PlayerId) {
    $.ajax({
        type: "get",
        url: '/Home/ShowPlayerProfile',
        data: { PlayerId: PlayerId },
        success: function (result) {
            $("#PlayerInformationDiv").html(result);
            $("#ShowPlayerProfileModal").modal("show");

        }
    });
}

function ViewTeamPlayers(TeamId) {
    $.ajax({
        type: "get",
        url: '/Home/ViewTeamPlayers',
        data: { TeamId: TeamId },
        success: function (result) {
            $("#TeamPlayersInformationDiv").html(result);
            $("#TeamPlayersPopup").modal("show");

        }
    });
}

function ViewVirtualTeamPlayers(TeamId) {
    $.ajax({
        type: "get",
        url: '/Home/ViewVirtualTeamPlayers',
        data: { TeamId: TeamId },
        success: function (result) {
            $("#TeamPlayersInformationDiv").html(result);
            $("#TeamPlayersPopup").modal("show");

        }
    });
}

function ShowResults(ScheduleId) {
    $.ajax({
        type: "get",
        url: '/Home/ShowResults',
        data: { ScheduleId: ScheduleId },
        success: function (result) {
            $("#ShowResultsDiv").html(result);
            $("#ShowResultsPopup").modal("show");

        }
    });
}

function DeleteTeam(TeamId) {
    swal({
        title: "Delete Team",
        text: "Are you sure! you want delete this Team?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((Confirm) => {
            if (Confirm) {
                $.ajax({
                    type: "get",
                    url: '/Home/DeleteTeam',
                    data: { TeamId: TeamId },
                    success: function (result) {
                        if (result.success) {
                            GetTeamData();
                            $.toaster({ priority: 'success', message: "Team removed successfully" });
                        }
                        else {
                            $.toaster({ priority: 'danger', message: result.message });
                        }
                    }
                });
            }
        });

}



function DeletePlayer(PlayerId, $this) {
    swal({
        title: "Delete Player",
        text: "Are you sure! you want delete this Player?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((Confirm) => {
            if (Confirm) {
                $.ajax({
                    type: "get",
                    url: '/Home/DeletePlayer',
                    data: { PlayerId: PlayerId },
                    success: function (result) {
                        if (result.success) {
                            $($this).parents('tr').remove();
                            $.toaster({ priority: 'success', message: "Player removed successfully" });
                        }
                        else {
                            $.toaster({ priority: 'danger', message: result.message });
                        }
                    }
                });
            }
        });

}


function GetLeagueTeamPopup() {
    $("#TeamNametxtbx").val($("#TeamNameContainer").text().trim());
    $("#LeagueTeamPopup").modal("show");
}

function SaveLeagueTeamInformation() {
    if ($("#TeamNametxtbx").val().trim() == "") {
        $.toaster({ priority: 'danger', message: "Please enter team title" });
        return;
    }

    $.ajax({
        type: "post",
        url: '/User/SaveTeamInformation',
        data: { TournamentId : $("#TournamentsDDL").val(), TeamTitle: $("#TeamNametxtbx").val(), TeamId: $("#TeamNameContainer").attr('data-id') },
        success: function (result) {
            if (result.success) {
                $("#TeamNameContainer").text($("#TeamNametxtbx").val());
                $("#TeamNameContainer").attr('data-id', result.TeamId);
                $("#LeagueTeamPopup").modal("hide");

                if ($("#CreateTeamDiv").length) {
                    $("#CreateTeamDiv").remove();
                }

                $(".DataContainer").removeClass('HideItem');
                $.toaster({ priority: 'success', message: "Team Information Saved Successfully" });
            }
            else {
                $.toaster({ priority: 'danger', message: result.message });
            }
        }
    });
}

function AddVirtualPlayerToTeam(PlayerId, $this) {
    $.ajax({
        type: "post",
        url: '/User/AddToTeam',
        data: { PlayerId: PlayerId, TeamId: $("#TeamNameContainer").attr('data-id') },
        success: function (result) {
            if (result.success) {
                $($this).parents('tr').remove();
                GetTeamMembers();
            }
            else {
                $.toaster({ priority: 'danger', message: result.message });
            }
        }
    });
}

function RemoveVirtualMember(PlayerId, $this) {
    $.ajax({
        type: "post",
        url: '/User/RemoveFromTeam',
        data: { PlayerId: PlayerId, TeamId: $("#TeamNameContainer").attr('data-id') },
        success: function (result) {
            if (result.success) {
                $($this).parents('tr').remove();
                $(".FilterBtn.FilterBtnActive").trigger('click')
            }
            else {
                $.toaster({ priority: 'danger', message: result.message });
            }
        }
    });
}

function GetTeamMembers() {
    $.ajax({
        type: "get",
        url: '/User/GetTeamMembers',
        data: { TeamId: $("#TeamNameContainer").attr('data-id') },
        success: function (result) {
            $("#TeamMembersListDiv").html(result);
        }
    });
}

function CallChangefunc(val) {

    $("#Teampartial").html('');
    if (val != "")
    {
        $.ajax({
            type: "post",
            url: '/User/GetTeamsPartial',
            data: { TournamentId: val },
            success: function (response) {

                $("#Teampartial").html(response);
            }
        });
    }


    //$.ajax({
    //    type: "post",
    //    url: '/User/GetTeamsInTournament',
    //    data: { p_strTournamentID: val },
    //    success: function (response) {
           
    //        $("#TeamsDDL").empty();
    //        if (response.data.length > 0) {
    //            $.each(response.data, function () {
    //                $("#TeamsDDL").append($("<option> </option>").val(this['Value']).html(this['Text']));
    //            });
                
    //        }
    //        else {
    //            $.toaster({ priority: 'danger', message: "No teams found for selected tournament." });
    //        }
    //    }
    //});
}


function GetLeaguesForTournament(v)
{
   // $("#LeaguesPartial").html('');
    if (v != "") {

        GetLeaguesGrid();
        GetEnrolledLeaguesGrid();
        GetOverallLeagues();
        $("#LeaguesPartial").show();

        //$.ajax({
        //    type: "post",
        //    url: '/User/GetLeaguesPartial',
        //    data: { TournamentId: v },
        //    success: function (response) {
        //        $("#LeaguesPartial").html(response);
        //    }
        //});
    }
    else
    {
        $("#LeaguesPartial").hide();
    }
}


function FilterData($this) {
    $(".FilterBtn").removeClass('FilterBtnActive');
    $($this).addClass('FilterBtnActive');

    $.ajax({
        type: "get",
        url: '/User/_PlayersSelectionPartial',
        data: { TournamentId :$("#TournamentsDDL").val(), PlayerType: $(".FilterBtn.FilterBtnActive").attr('data-id'), TeamId: $("#TeamsDDL").val(), SelectedTeamId: $("#TeamNameContainer").attr('data-id') },
        success: function (result) {
            $("#PlayersSelectionDiv").html(result);
        }
    });
}

$(document).on("change", "#TeamsDDL", function () {
    $(".FilterBtn.FilterBtnActive").trigger('click');
});


function SetAsCaptain(PlayerId) {
    $.ajax({
        type: "post",
        url: '/User/SetAsCaptain',
        data: { PlayerId: PlayerId, TeamId: $("#TeamNameContainer").attr('data-id') },
        success: function (result) {
            if (result.success) {
                GetTeamMembers();
            }
            else {
                $.toaster({ priority: 'danger', message: result.message });
            }
        }
    });
}

function SetAsManOfMatch(PlayerId) {
    $.ajax({
        type: "post",
        url: '/User/SetAsManOfMatch',
        data: { PlayerId: PlayerId, TeamId: $("#TeamNameContainer").attr('data-id') },
        success: function (result) {
            if (result.success) {
                GetTeamMembers();
            }
            else {
                $.toaster({ priority: 'danger', message: result.message });
            }
        }
    });
}
var m_boolCreateLeaguePopUp = false;
var m_boolJoinLeaguePopUp = false;
var m_intLeagueID = 0;
function CheckLeagueTeam() {
    $.ajax({
        type: "get",
        url: '/User/CheckTeam',
        success: function (result) {
            if (result.success) {
                CheckTeamHasPlayersandMOM();
            }
            else {
                $.toaster({ priority: 'danger', message: "Please create your team first" });
            }
        }
    });
}

function CheckTeamHasPlayersandMOM() {
    $.ajax({
        type: "get",
        url: '/User/CheckTeamHasPlayersandMOM',
        success: function (result) {
            if (result.success) {
                if (m_boolCreateLeaguePopUp) {
                    m_boolCreateLeaguePopUp = false;
                    GetLeaguePopup(0);
                }
                else {
                    if (m_boolJoinLeaguePopUp) {
                        m_boolJoinLeaguePopUp = false;
                        $("#HiddenForJoinLeague").val(m_intLeagueID);
                        $("#JoinLeaguePopup").modal('show');
                    }
                }
            }
            else {
                $.toaster({ priority: 'danger', message: result.message });
            }
        }
    });
}

function GetLeaguePopup(LeagueId) {
    $("#LeagueIdHidden").val(LeagueId);

    if (LeagueId != 0) {
        $.ajax({
            type: "get",
            data: { LeagueId: LeagueId },
            url: '/User/GetLeagueTitle',
            success: function (result) {
                $("#LeagueNameTxtBx").val(result.LeagueTitle);
                $("#LeaguePasswordBox").val(result.Password);
            }
        });
    }

    $("#LeaguePopup").modal("show");
}

function SaveLeagueInformation() {

    if ($("#LeagueNameTxtBx").val().trim() == "" || $("#LeaguePasswordBox").val().trim() == "") {
        $.toaster({ priority: 'danger', message: "Please enter league details" });
        return
    }

    $.ajax({
        type: "post",
        data: {TournamentId: $("#TournamentsDDL2").val(), LeagueId: $("#LeagueIdHidden").val(), LeagueTitle: $("#LeagueNameTxtBx").val(), Password: $("#LeaguePasswordBox").val() },
        url: '/User/CreateLeague',
        success: function (result) {
            if (result.success) {
                GetLeaguesGrid();
                $("#LeagueNameTxtBx").val('');
                $("#LeaguePasswordBox").val('');
                $("#LeaguePopup").modal("hide");
                $.toaster({ priority: 'success', message: result.message });
            }
            else {
                $.toaster({ priority: 'danger', message: result.message });
            }
        }
    });
}


function GetLeaguesGrid() {
    $.ajax({
        type: "get",
        url: '/User/GetLeaguesGrid',
        data: { TournamentId: $("#TournamentsDDL2").val() },
        success: function (result) {
            $("#LeaguesDiv").html(result);
        }
    });
}


function GetInvitesPopup(LeagueId) {
    $("#HiddenForLeagueidEmails").val(LeagueId);
    $("#InvitesPopup").modal('show');
}

function SendInvites() {
    if ($("#EmailsTxtbx").val().trim() == "") {
        $.toaster({ priority: 'danger', message: "Please enter emails" });
        return
    }

    $.ajax({
        type: "post",
        data: { Emails: $("#EmailsTxtbx").val(), LeagueId: $("#HiddenForLeagueidEmails").val() },
        url: '/User/SendInvites',
        success: function (result) {
            if (result.success) {
                $("#EmailsTxtbx").val('');
                $("#InvitesPopup").modal("hide");
                $.toaster({ priority: 'success', message: result.message });
            }
            else {
                $.toaster({ priority: 'danger', message: result.message });
            }
        }
    });

}

function GetEnrolledLeaguesGrid() {
    $.ajax({
        type: "get",
        url: '/User/GetEnrolledLeaguesGrid',
        data: { TournamentId: $("#TournamentsDDL2").val() },
        success: function (result) {
            $("#EnrolledDiv").html(result);
        }
    });
}


function GetOverallLeagues() {
    $.ajax({
        type: "get",
        url: '/User/GetOverallLeagues',
        data: { TournamentId: $("#TournamentsDDL2").val() },
        success: function (result) {
            $("#OverallDiv").html(result);
        }
    });
}

function GetJoinPopup(LeagueId) {
    m_intLeagueID = parseInt(LeagueId);
    m_boolJoinLeaguePopUp = true;
    m_boolCreateLeaguePopUp = false;
    CheckLeagueTeam();
}

function JoinLeague() {
    if ($("#JoinLeaguePasswordTxtbx").val().trim() == "") {
        $.toaster({ priority: 'danger', message: "Please enter league password to join" });
        return
    }

    $.ajax({
        type: "post",
        data: { Password: $("#JoinLeaguePasswordTxtbx").val(), LeagueId: $("#HiddenForJoinLeague").val() },
        url: '/User/JoinLeague',
        success: function (result) {
            if (result.success) {
                $("#JoinLeaguePopup").modal("hide");
                $.toaster({ priority: 'success', message: result.message });
                GetOverallLeagues();
                GetEnrolledLeaguesGrid();
                $("#JoinLeaguePasswordTxtbx").val('');
            }
            else {
                $.toaster({ priority: 'danger', message: result.message });
            }
        }
    });

}

function JoinOwnLeague(LeagueId) {
    $("#HiddenForJoinLeague").val(LeagueId);
    $.ajax({
        type: "post",
        data: { LeagueId: $("#HiddenForJoinLeague").val() },
        url: '/User/JoinOwnLeague',
        success: function (result) {
            if (result.success) {
                $.toaster({ priority: 'success', message: result.message });
                GetLeaguesGrid();
                GetOverallLeagues();
                GetEnrolledLeaguesGrid();
            }
            else {
                $.toaster({ priority: 'danger', message: result.message });
            }
        }
    });

}

function CheckForgotPasswordValidation() {
    var l_strEmail = $("#fpEmail").val();
    $.ajax({
        type: "post",
        data: { p_strEmail: $("#fpEmail").val() },
        url: '/User/JoinOwnLeague',
        success: function (result) {
            if (result.success) {
                $.toaster({ priority: 'success', message: result.message });
                GetLeaguesGrid();
                GetOverallLeagues();
                GetEnrolledLeaguesGrid();
            }
            else {
                $.toaster({ priority: 'danger', message: result.message });
            }
        }
    });
}

function UpdateUserDetails() {

    $.ajax({
        type: "post",
        data: {  p_strPhoneNumber: $("#PhoneNumber").val(), p_strPassword: $("#Password").val(), p_strCountry: $("#Country").val(), p_strFullName: $("#FullName").val() },
        url: '/Account/Profile',
        success: function (result) {
            if (result.success) {
                $.toaster({ priority: 'success', message: result.message });
            }
            else {
                $.toaster({ priority: 'danger', message: result.message });
            }
        }
    });

}


