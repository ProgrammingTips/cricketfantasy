﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CricketFantasy.Startup))]
namespace CricketFantasy
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
