﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataTransferObjects.Models
{
    public class Players
    {
        public int PlayerId { get; set; }

        [Required(ErrorMessage = "Please enter Player Name")]
        public string PlayerName { get; set; }

        [Required(ErrorMessage = "Please enter Player Code")]
        public string PlayerCode { get; set; }

        [Required(ErrorMessage = "Please enter Player Type")]
        public string PlayerType { get; set; }

        public bool isCaptain { get; set; }
    }
}