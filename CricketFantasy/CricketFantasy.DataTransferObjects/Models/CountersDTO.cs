﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataTransferObjects.Models
{
    public class CountersDTO
    {
        public int TotalTeams { get; set; }
        public int TotalVirtualTeams { get; set; }
        public int TotalTournaments { get; set; }
        public int TotalPlayers { get; set; }
        public int TotalUsers { get; set; }
    }
}