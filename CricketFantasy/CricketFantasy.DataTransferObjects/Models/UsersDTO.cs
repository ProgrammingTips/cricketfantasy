﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataTransferObjects.Models
{
    public class UsersDTO
    {
        public string UserId { get; set; }

        public string Email { get; set; }

        public string RoleTitle { get; set; }

        public string UserName { get; set; }
    }

    public class TeamDTO
    {
        public int TeamId { get; set; }

        public string TeamName { get; set; }

        public bool isActive { get; set; }

        public string Country { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? PlayersCount { get; set; }

        public int? TotalPoints { get; set; }
    }
}