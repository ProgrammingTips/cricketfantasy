﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CricketFantasy.DataTransferObjects.Models
{
    public class CreateUserDTO
    {
        public string UserId { get; set; }

        [Required(ErrorMessage = "Please enter Username")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter Password")]
        public string Password { get; set; }
        
        public string UserRoleId { get; set; }

        public List<RolesDTO> RolesList { get; set; }
    }
}