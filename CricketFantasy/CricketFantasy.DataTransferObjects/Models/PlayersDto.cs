﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataTransferObjects.Models
{
    public class PlayersDto
    {
        public PlayersDto()
        {
            StaticsList = new List<PlayerStatics>();
        }
        public int PlayerId { get; set; }

        [Required(ErrorMessage = "Please enter PlayerName")]
        public string PlayerName { get; set; }

        public int Points { get; set; }

        public string PlayerCode { get; set; }

        [Required(ErrorMessage = "Please enter Player Type")]
        public string PlayerType { get; set; }

        public bool isCaptain { get; set; }

        public int TeamId { get; set; }

        public string TeamName { get; set; }

        public string Tournament { get; set; }

        public List<PlayerStatics> StaticsList { get; set; }

        public string ImagePath { get; set; }

        public bool HasImage { get; set; }
        public bool IsInCurrentTeam { get; set; }

        public int? PlayerValue { get; set; }

        public bool IsManOfMatch { get; set; }
    }

    public class PlayerStatics
    {
        public PlayerStatics()
        {
            Statics = new StaticsList();
        }
        public string MatchType { get; set; }
        public StaticsList Statics { get; set; }
    }

    public class StaticsList
    {
        public string MatchType { get; set; }
        public int? TotalMatches { get; set; }
        public int? InningsBatted { get; set; }
        public int? RunsScored { get; set; }
        public string Average { get; set; }
        public int? Hundred { get; set; }
        public int? Fifty { get; set; }
        public int? Fours { get; set; }
        public int? Sixes { get; set; }
        public int? BallsBowled { get; set; }
        public int? WicketsTaken { get; set; }
        public string Economy { get; set; }
        public int? FourWicketsInMatch { get; set; }
        public int? FiveWicketsInMatch { get; set; }
        public int? TenWicketsInMatch { get; set; }
        public int? HighestScore { get; set; }
        public int? InningsBowled { get; set; }
        public int? NotOuts { get; set; }
        public int? BallsFaced { get; set; }
        public string StrikeRate { get; set; }
        public int? CatchesTaken { get; set; }
        public int? RunsGiven { get; set; }
    }

}