﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataTransferObjects.Models
{
    public class CreateTeamDto
    {
        public int TeamId { get; set; }

        [Required(ErrorMessage =  "Please enter Team Name")]
        [System.Web.Mvc.Remote("ExistingTeam", "Team", ErrorMessage = "Team already exists", AdditionalFields = "TeamId")]
        public string TeamName { get; set; }

        [Required(ErrorMessage = "Please enter Country name")]
        public string CountryName { get; set; }    

        public List<Players> Player{ get; set; }

        public CreateTeamDto()
        {
            Player = new List<Players>();
        }
    }
}