﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataTransferObjects.Models
{
    public class MatchResultsDTO
    {
        public int MatchId { get; set; }

        public int TeamAScore { get; set; }
        public string TeamAOversPlayed { get; set; }
        public int TeamAPlayersOut { get; set; }

        public int TeamBScore { get; set; }
        public string TeamBOversPlayed { get; set; }
        public int TeamBPlayersOut { get; set; }

        public int Winner { get; set; }
        public string WinCondition { get; set; }

        public string TeamAExtras { get; set; }
        public string TeamADidntBat { get; set; }
        public string TeamAFallofWickets { get; set; }

        public string TeamBExtras { get; set; }
        public string TeamABidntBat { get; set; }
        public string TeamBFallofWickets { get; set; }

        public string TeamAInningDetails { get; set; }
        public string TeamBInningDetails { get; set; }

        public string TeamABowlingDetails { get; set; }
        public string TeamBBowlingDetails { get; set; }
    }

    public class MatchDetailsDTO
    {
        public int PlayerId { get; set; }
        public string PlayerName { get; set; }
        public string OutByPlayerName { get; set; }
        public string OutStatus { get; set; }
        public string OutType { get; set; }
        public int OutByPlayerId { get; set; }
        public int Runs { get; set; }
        public int Balls { get; set; }
        [Display(Name = "Strike Rate")]
        public int Minutes { get; set; }
        public int Fours { get; set; }
        public int Sixes { get; set; }
        //why strike rate is commented in matchresult dto
        //public string StrikeRate { get; set; }
        public int Overs { get; set; }
        public int Maiden { get; set; }
        public int Wickets { get; set; }
        public int Economy { get; set; }
        public int Wides { get; set; }
        public int NoBalls { get; set; }
        public int Inning { get; set; }
    }
}