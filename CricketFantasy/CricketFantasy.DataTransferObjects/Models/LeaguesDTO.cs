﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataTransferObjects.Models
{
    public class LeaguesDTO
    {
        public int LeagueId { get; set; }
        public string LeagueTitle { get; set; }
        public int TotalTeams { get; set; }
        public string CreatedDate { get; set; }
        public string Password { get; set; }
        public bool IsLeagueCommissioner { get; set; }

    }
}