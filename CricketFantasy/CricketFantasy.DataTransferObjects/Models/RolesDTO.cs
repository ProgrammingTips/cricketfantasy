﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataTransferObjects.Models
{
    public class RolesDTO
    {
        public string RoleId { get; set; }

        public string RoleName { get; set; }
    }
}