﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataTransferObjects.Models
{
    public class TournamentsDTO
    {
        public TournamentsDTO()
        {
            MatchSchedules = new List<MatchScheduleDTO>();
            TeamsList = new List<TeamDTO>();
        }

        public int TournamentId { get; set; }

        public string TournamentName { get; set; }

        public string TournamentType { get; set; }

        public string TournamentCode { get; set; }

        public string Venue { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? TeamBudget { get; set; }
        
        public int? NoOfChangesAllowed { get; set; }     

        public List<MatchScheduleDTO> MatchSchedules { get; set; }

        public List<TeamDTO> TeamsList { get; set; }

        public int? NumberofTeams { get; set; }
         
    }

    public class MatchScheduleDTO
    {
        public int MatchScheduleId { get; set; }

        public int TeamAId { get; set; }

        public int TeamBId { get; set; }

        public string TeamAName { get; set; }

        public string TeamBName { get; set; }

        public DateTime? ScheduleDateTime { get; set; }

        public string ScheduleDate { get; set; }

        public string TournamentTitle { get; set; }

        public string Location { get; set; }

        public List<PlayersDto> TeamAPlayers { get; set; }

        public List<PlayersDto> TeamBPlayers { get; set; }

        public string Status { get; set; }
    }

}