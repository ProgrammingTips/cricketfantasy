﻿using CricketFantasy.DataLayer.Model;
using CricketFantasy.DataLayer.Modules;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataLayer.Actions
{
    public class LeagueTeamActions : BaseActions<LeagueTeam>
    {
        public override void Add(LeagueTeam t)
        {
            ctx.LeagueTeams.Add(t);
            Save();
        }

        public override void Change(LeagueTeam t)
        {
            Save();
        }

        public override void Delete(string id)
        {
        }
        public override LeagueTeam Get(int id)
        {
            return ctx.LeagueTeams.First(x => x.Id == id);
        }

        public override void Delete(int id)
        {
            ctx.LeagueTeams.Where(x => x.Id == id).Delete();
            Save();
        }

        public override LeagueTeam Get(string id)
        {
            return ctx.LeagueTeams.First(x => x.Id == Convert.ToInt32(id));
        }

        public override IQueryable<LeagueTeam> GetAll()
        {
            return ctx.LeagueTeams;
        }
    }
}