﻿using CricketFantasy.DataLayer.Model;
using CricketFantasy.DataLayer.Modules;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataLayer.Actions
{
    public class TeamPlayerActions : BaseActions<TeamPlayer>
    {
        public override void Add(TeamPlayer t)
        {
            ctx.TeamPlayers.Add(t);
            Save();
        }

        public override void Change(TeamPlayer t)
        {
            Save();
        }

        public override void Delete(string id)
        {
        }
        public override TeamPlayer Get(int id)
        {
            return ctx.TeamPlayers.First(x => x.Id == id);
        }

        public override void Delete(int id)
        {
            ctx.TeamPlayers.Where(x => x.Id == id).Delete();
            Save();
        }

        public override TeamPlayer Get(string id)
        {
            return ctx.TeamPlayers.First(x => x.Id == Convert.ToInt32(id));
        }

        public override IQueryable<TeamPlayer> GetAll()
        {
            return ctx.TeamPlayers;
        }
    }
}