﻿using CricketFantasy.DataLayer.Model;
using CricketFantasy.DataLayer.Modules;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataLayer.Actions
{
    public class VirtualPlayerActions : BaseActions<VirtualTeamPlayer>
    {
        public override void Add(VirtualTeamPlayer t)
        {
            ctx.VirtualTeamPlayers.Add(t);
            Save();
        }

        public override void Change(VirtualTeamPlayer t)
        {
            Save();
        }

        public override void Delete(string id)
        {
        }
        public override VirtualTeamPlayer Get(int id)
        {
            return ctx.VirtualTeamPlayers.First(x => x.Id == id);
        }

        public override void Delete(int id)
        {
            ctx.VirtualTeamPlayers.Where(x => x.Id == id).Delete();
            Save();
        }

        public override VirtualTeamPlayer Get(string id)
        {
            return ctx.VirtualTeamPlayers.First(x => x.Id == Convert.ToInt32(id));
        }

        public override IQueryable<VirtualTeamPlayer> GetAll()
        {
            return ctx.VirtualTeamPlayers;
        }
    }
}