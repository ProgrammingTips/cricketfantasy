﻿using CricketFantasy.DataLayer.Model;
using CricketFantasy.DataLayer.Modules;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataLayer.Actions
{
    public class MatchResultActions : BaseActions<MatchResult>
    {
        public override void Add(MatchResult t)
        {
            ctx.MatchResults.Add(t);
            Save();
        }

        public override void Change(MatchResult t)
        {
            Save();
        }

        public override void Delete(string id)
        {
        }
        public override MatchResult Get(int id)
        {
            return ctx.MatchResults.First(x => x.Id == id);
        }

        public override void Delete(int id)
        {
            ctx.MatchResults.Where(x => x.Id == id).Delete();
            Save();
        }

        public override MatchResult Get(string id)
        {
            return ctx.MatchResults.First(x => x.Id == Convert.ToInt32(id));
        }

        public override IQueryable<MatchResult> GetAll()
        {
            return ctx.MatchResults;
        }
    }
}