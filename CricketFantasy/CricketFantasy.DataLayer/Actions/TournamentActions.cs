﻿using CricketFantasy.DataLayer.Model;
using CricketFantasy.DataLayer.Modules;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataLayer.Actions
{
    public class TournamentActions : BaseActions<Tournament>
    {
        public override void Add(Tournament t)
        {
            ctx.Tournaments.Add(t);
            Save();
        }

        public override void Change(Tournament t)
        {
            Save();
        }

        public override void Delete(string id)
        {
           // ctx.Tournaments.Where(x => x.Id == id).Delete();
            Save();
        }

        public override Tournament Get(string id)
        {
            return null;
        }

        public override IQueryable<Tournament> GetAll()
        {
            return ctx.Tournaments;
        }

        public override void Delete(int id)
        {
            ctx.Tournaments.Where(x => x.Id == id).Delete();
            Save();
        }

        public override Tournament Get(int id)
        {
            return ctx.Tournaments.First(x => x.Id == id);
        }
    }

}