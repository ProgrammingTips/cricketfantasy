﻿using CricketFantasy.DataLayer.Model;
using CricketFantasy.DataLayer.Modules;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataLayer.Actions
{
    public class RoleActions : BaseActions<AspNetRole>
    {
        public override void Add(AspNetRole t)
        {
            ctx.AspNetRoles.Add(t);
            Save();
        }

        public override void Change(AspNetRole t)
        {
            Save();
        }

        //Action Not in Use For Users 
        public override void Delete(string id)
        {
            ctx.AspNetRoles.Where(x => x.Id == id).Delete();
            Save();
        }
        
        public override AspNetRole Get(string id)
        {
            return ctx.AspNetRoles.First(x => x.Id == id);
        }

        public override IQueryable<AspNetRole> GetAll()
        {
            return ctx.AspNetRoles;
        }

        public override void Delete(int id)
        {
        }
        public override AspNetRole Get(int id)
        {
            return ctx.AspNetRoles.First(x => x.Id == id.ToString());
        }
    }
}