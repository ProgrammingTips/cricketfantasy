﻿using CricketFantasy.DataLayer.Model;
using CricketFantasy.DataLayer.Modules;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataLayer.Actions
{
    public class MatchScheduleActions : BaseActions<MatchSchedule>
    {
        public override void Add(MatchSchedule t)
        {
            ctx.MatchSchedules.Add(t);
            Save();
        }

        public override void Change(MatchSchedule t)
        {
            Save();
        }

        public override void Delete(string id)
        {
            // ctx.Tournaments.Where(x => x.Id == id).Delete();
            Save();
        }

        public override MatchSchedule Get(string id)
        {
            return null;
        }

        public override IQueryable<MatchSchedule> GetAll()
        {
            return ctx.MatchSchedules;
        }

        public override void Delete(int id)
        {
            ctx.MatchSchedules.Where(x => x.Id == id).Delete();
            Save();
        }

        public override MatchSchedule Get(int id)
        {
            return ctx.MatchSchedules.First(x => x.Id == id);
        }

        public void DeleteResults(int matchId)
        {
            ctx.BowlerStatistics.Where(x => x.MatchScheduleId == matchId).Delete();
            ctx.BowlerStatistics.Where(x => x.MatchScheduleId == matchId).Delete();
        }
    }
}