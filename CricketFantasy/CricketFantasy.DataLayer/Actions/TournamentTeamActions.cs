﻿using CricketFantasy.DataLayer.Model;
using CricketFantasy.DataLayer.Modules;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataLayer.Actions
{
    public class TournamentTeamActions : BaseActions<TournamentTeam>
    {
        public override void Add(TournamentTeam t)
        {
            ctx.TournamentTeams.Add(t);
            Save();
        }

        public override void Change(TournamentTeam t)
        {
            Save();
        }

        public override void Delete(string id)
        {
            // ctx.Tournaments.Where(x => x.Id == id).Delete();
            Save();
        }

        public override TournamentTeam Get(string id)
        {
            return null;
        }

        public override IQueryable<TournamentTeam> GetAll()
        {
            return ctx.TournamentTeams;
        }

        public override void Delete(int id)
        {
            ctx.TournamentTeams.Where(x => x.Id == id).Delete();
            Save();
        }

        public override TournamentTeam Get(int id)
        {
            return ctx.TournamentTeams.First(x => x.Id == id);
        }
    }
}