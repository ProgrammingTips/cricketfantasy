﻿using CricketFantasy.DataLayer.Model;
using CricketFantasy.DataLayer.Modules;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataLayer.Actions
{
    public class BowlerStaticsActions : BaseActions<BowlerStatistic>
    {
        public override void Add(BowlerStatistic t)
        {
            ctx.BowlerStatistics.Add(t);
            Save();
        }

        public override void Change(BowlerStatistic t)
        {
            Save();
        }

        public override void Delete(string id)
        {
        }
        public override BowlerStatistic Get(int id)
        {
            return ctx.BowlerStatistics.First(x => x.Id == id);
        }

        public override void Delete(int id)
        {
            ctx.BowlerStatistics.Where(x => x.Id == id).Delete();
            Save();
        }

        public override BowlerStatistic Get(string id)
        {
            return ctx.BowlerStatistics.First(x => x.Id == Convert.ToInt32(id));
        }

        public override IQueryable<BowlerStatistic> GetAll()
        {
            return ctx.BowlerStatistics;
        }
    }
}