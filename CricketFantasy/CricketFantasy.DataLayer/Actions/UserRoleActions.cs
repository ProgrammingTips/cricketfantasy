﻿using CricketFantasy.DataLayer.Model;
using CricketFantasy.DataLayer.Modules;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataLayer.Actions
{
    public class UserRoleActions : BaseActions<AspNetUserRole>
    {
        public override void Add(AspNetUserRole t)
        {
            ctx.AspNetUserRoles.Add(t);
            Save();
        }

        public override void Change(AspNetUserRole t)
        {
            Save();
        }

        public override void Delete(string id)
        {
            ctx.AspNetUserRoles.Where(x => x.UserId == id).Delete();
            Save();
        }

        public override AspNetUserRole Get(string id)
        {
            return ctx.AspNetUserRoles.First(x => x.UserId == id);
        }

        public override IQueryable<AspNetUserRole> GetAll()
        {
            return ctx.AspNetUserRoles;
        }

        public override void Delete(int id)
        {
        }
        public override AspNetUserRole Get(int id)
        {
            return ctx.AspNetUserRoles.First(x => x.Id == id);
        }
    }
    }