﻿using CricketFantasy.DataLayer.Model;
using CricketFantasy.DataLayer.Modules;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataLayer.Actions
{
    public class TeamManagerActions : BaseActions<TeamManager>
    {
        public override void Add(TeamManager t)
        {
            ctx.TeamManagers.Add(t);
            Save();
        }

        public override void Change(TeamManager t)
        {
            Save();
        }

        public override void Delete(string id)
        {
        }
        public override TeamManager Get(int id)
        {
            return ctx.TeamManagers.First(x => x.Id == id);
        }

        public override void Delete(int id)
        {
            ctx.TeamManagers.Where(x => x.Id == id).Delete();
            Save();
        }

        public override TeamManager Get(string id)
        {
            return ctx.TeamManagers.First(x => x.Id == Convert.ToInt32(id));
        }

        public override IQueryable<TeamManager> GetAll()
        {
            return ctx.TeamManagers;
        }
    }
}