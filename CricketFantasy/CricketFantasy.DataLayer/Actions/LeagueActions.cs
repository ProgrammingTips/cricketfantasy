﻿using CricketFantasy.DataLayer.Model;
using CricketFantasy.DataLayer.Modules;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataLayer.Actions
{
    public class LeagueActions : BaseActions<League>
    {
        public override void Add(League t)
        {
            ctx.Leagues.Add(t);
            Save();
        }

        public override void Change(League t)
        {
            Save();
        }

        public override void Delete(string id)
        {
        }
        public override League Get(int id)
        {
            return ctx.Leagues.First(x => x.Id == id);
        }

        public override void Delete(int id)
        {
            ctx.Leagues.Where(x => x.Id == id).Delete();
            Save();
        }

        public override League Get(string id)
        {
            return ctx.Leagues.First(x => x.Id == Convert.ToInt32(id));
        }

        public override IQueryable<League> GetAll()
        {
            return ctx.Leagues;
        }
    }
}