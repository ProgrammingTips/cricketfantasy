﻿using CricketFantasy.DataLayer.Model;
using CricketFantasy.DataLayer.Modules;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataLayer.Actions
{
    public class BatsmenActions : BaseActions<BatsmenStatistic>
    {
        public override void Add(BatsmenStatistic t)
        {
            ctx.BatsmenStatistics.Add(t);
            Save();
        }

        public override void Change(BatsmenStatistic t)
        {
            Save();
        }

        public override void Delete(string id)
        {
        }
        public override BatsmenStatistic Get(int id)
        {
            return ctx.BatsmenStatistics.First(x => x.Id == id);
        }

        public override void Delete(int id)
        {
            ctx.BatsmenStatistics.Where(x => x.Id == id).Delete();
            Save();
        }

        public override BatsmenStatistic Get(string id)
        {
            return ctx.BatsmenStatistics.First(x => x.Id == Convert.ToInt32(id));
        }

        public override IQueryable<BatsmenStatistic> GetAll()
        {
            return ctx.BatsmenStatistics;
        }
    }
}