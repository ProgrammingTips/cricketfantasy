﻿using CricketFantasy.DataLayer.Model;
using CricketFantasy.DataLayer.Modules;
using CricketFantasy.DataTransferObjects.Models;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataLayer.Actions
{
    public class Teamaction : BaseActions<Team>
    {
        public override void Add(Team t)
        {
            ctx.Teams.Add(t);
            Save();
        }

        public override void Change(Team t)
        {
            Save();
        }

        public override void Delete(string id)
        {
            ctx.Teams.Where(x => x.Id.ToString() == id).Delete();
            Save();
        }

        public override Team Get(string id)
        {
            return ctx.Teams.First(x => x.Id.ToString() == id);
        }

        public override IQueryable<Team> GetAll()
        {
            return ctx.Teams;
        }

        public override void Delete(int id)
        {
            ctx.Teams.Where(x=>x.Id == id).Delete();
        }

        public override Team Get(int id)
        {
            return ctx.Teams.First(x => x.Id == id);
        }
    }
}