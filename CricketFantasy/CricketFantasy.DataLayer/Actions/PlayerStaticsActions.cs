﻿using CricketFantasy.DataLayer.Model;
using CricketFantasy.DataLayer.Modules;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataLayer.Actions
{
    public class PlayerStaticsActions : BaseActions<PlayerStatic>
    {
        public override void Add(PlayerStatic t)
        {
            ctx.PlayerStatics.Add(t);
            Save();
        }

        public override void Change(PlayerStatic t)
        {
            Save();
        }

        public override void Delete(string id)
        {
        }
        public override PlayerStatic Get(int id)
        {
            return ctx.PlayerStatics.First(x => x.Id == id);
        }

        public override void Delete(int id)
        {
            ctx.PlayerStatics.Where(x => x.Id == id).Delete();
            Save();
        }

        public override PlayerStatic Get(string id)
        {
            return ctx.PlayerStatics.First(x => x.Id == Convert.ToInt32(id));
        }

        public override IQueryable<PlayerStatic> GetAll()
        {
            return ctx.PlayerStatics;
        }

        public void DeleteStatics(int playerId)
        {
            ctx.PlayerStatics.Where(x => x.PlayerId == playerId).Delete();
        }
    }
}