﻿using CricketFantasy.DataLayer.Model;
using CricketFantasy.DataLayer.Modules;
using CricketFantasy.DataTransferObjects.Models;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataLayer.Actions
{
    public class UserActions : BaseActions<AspNetUser>
    {
        public override void Add(AspNetUser t)
        {
            ctx.AspNetUsers.Add(t);
            Save();
        }

        public override void Change(AspNetUser t)
        {
            Save();
        }
        
        public override void Delete(string id)
        {
            ctx.AspNetUsers.Where(x => x.Id == id).Delete();
            Save();
        }
        
        public override AspNetUser Get(string id)
        {
            return ctx.AspNetUsers.First(x => x.Id == id);
        }

        public override IQueryable<AspNetUser> GetAll()
        {
            return ctx.AspNetUsers;
        }

        public override void Delete(int id)
        {
        }
        public override AspNetUser Get(int id)
        {
            return ctx.AspNetUsers.First(x => x.Id == id.ToString());
        }

        public void AddOrUpdateUser(CreateUserDTO model)
        {
            if(model.UserId != "0")
            {
                var user = ctx.AspNetUsers.Where(x => x.Id == model.UserId).First();
                user.PasswordHash = model.Password;
                user.Email = model.Email;
                user.UserName = model.UserName;
                Change(user);
            }
            else
            {
                Guid id = Guid.NewGuid();
                AspNetUser user = new AspNetUser();
                user.Id = id.ToString();
                user.Email = model.Email;
                user.PasswordHash = model.Password;
                user.UserName = model.UserName;
                Add(user);
                model.UserId = id.ToString();        
            }

            AddOrUpdateUserRole(model.UserId, model.UserRoleId);
        }

        public void AddOrUpdateUserRole(string userId, string userRoleId)
        {
            if(string.IsNullOrEmpty(userRoleId))
            {
                var role = ctx.AspNetUserRoles.Where(x => x.UserId == userId).FirstOrDefault();
                if(role!=null)
                {
                    ctx.AspNetUserRoles.Remove(role);
                    ctx.SaveChanges();

                }
            }
            else
            {
                var role = ctx.AspNetUserRoles.Where(x => x.UserId == userId).FirstOrDefault();
                if (role != null)
                {
                    role.RoleId = userRoleId;
                    DataAccess.Instance.UserRoleAction.Change(role);
                }
                else
                {
                    AspNetUserRole newrole = new AspNetUserRole();
                    newrole.RoleId = userRoleId;
                    newrole.UserId = userId;
                    DataAccess.Instance.UserRoleAction.Add(newrole);
                }
            }
        }
    }
}