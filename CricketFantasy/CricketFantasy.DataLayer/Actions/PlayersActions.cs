﻿using CricketFantasy.DataLayer.Model;
using CricketFantasy.DataLayer.Modules;
using CricketFantasy.DataTransferObjects.Models;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataLayer.Actions
{
    public class PlayersActions : BaseActions<Player>
    {
        public override void Add(Player t)
        {
            ctx.Players.Add(t);
            Save();
        }

        public override void Change(Player t)
        {
            Save();
        }

        public override void Delete(string id)
        {
        }
        public override Player Get(int id)
        {
            return ctx.Players.First(x => x.Id == id);
        }

        public override void Delete(int id)
        {
            ctx.Players.Where(x => x.Id == id).Delete();
            Save();
        }

        public override Player Get(string id)
        {
            return ctx.Players.First(x => x.Id == Convert.ToInt32(id));
        }

        public override IQueryable<Player> GetAll()
        {
            return ctx.Players;
        }
    }
}