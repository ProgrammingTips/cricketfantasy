﻿using CricketFantasy.DataLayer.Actions;
using CricketFantasy.DataLayer.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataLayer
{
    public class DataAccess
    {
        internal Model.CricketFantasyEntities _ctx = null;
        static DataAccess self = null;
        public object ModulePermission;

        private DataAccess()
        {
            var tmp = new ConnectionStringBuilder(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            _ctx = new Model.CricketFantasyEntities(tmp.CricketFantasyString);

            // temporarily dissabling the EF entity validation - it should be removed in the next code sync.
            _ctx.Configuration.ValidateOnSaveEnabled = false;
        }
        public static DataAccess Instance
        {
            get
            {
                var context = HttpContext.Current;
                if (context != null)
                {
                    const string kApplicationSettings = "ApplicationObject";
                    if (context != null && context.Items[kApplicationSettings] != null)
                    {
                        var da = context.Items[kApplicationSettings] as DataAccess;
                        return da;
                    }

                    self = new DataAccess();
                    context.Items[kApplicationSettings] = self;
                }
                else
                {
                    self = new DataAccess();
                }
                return self;
            }
        }
        public void Save()
        {
            _ctx.SaveChanges();
        }
        internal Model.CricketFantasyEntities Ctx { get { return _ctx; } }
        public void Dispose()
        {
            if (self != null)
            {
                const string kApplicationSettings = "ApplicationObject";
                var context = HttpContext.Current;
                if (context != null && context.Items[kApplicationSettings] != null)
                    context.Items[kApplicationSettings] = null;
                _ctx.Dispose();
                _ctx = null;
                GC.SuppressFinalize(this);
                self = null;
            }
        }

        public UserActions UsersAction
        {
            get
            {
                UserActions Ans = new UserActions();
                Ans.SetContainer(this);
                return Ans;
            }
        }

        public RoleActions RoleAction
        {
            get
            {
                RoleActions Ans = new RoleActions();
                Ans.SetContainer(this);
                return Ans;
            }
        }

        public UserRoleActions UserRoleAction
        {
            get
            {
                UserRoleActions Ans = new UserRoleActions();
                Ans.SetContainer(this);
                return Ans;
            }
        }

        public Teamaction TeamAction
        {
            get
            {
                Teamaction Ans = new Teamaction();
                Ans.SetContainer(this);
                return Ans;
            }
        }

        public PlayersActions PlayersActions
        {
            get
            {
                PlayersActions Ans = new PlayersActions();
                Ans.SetContainer(this);
                return Ans;
            }
        }

        public TournamentActions TournamentAction
        {
            get
            {
                TournamentActions Ans = new TournamentActions();
                Ans.SetContainer(this);
                return Ans;
            }
        }

        public TournamentTeamActions TournamentTeamAction
        {
            get
            {
                TournamentTeamActions Ans = new TournamentTeamActions();
                Ans.SetContainer(this);
                return Ans;
            }
        }

        public MatchScheduleActions MatchScheduleAction
        {
            get
            {
                MatchScheduleActions Ans = new MatchScheduleActions();
                Ans.SetContainer(this);
                return Ans;
            }
        }

        public TeamPlayerActions TeamPlayerAction
        {
            get
            {
                TeamPlayerActions Ans = new TeamPlayerActions();
                Ans.SetContainer(this);
                return Ans;
            }
        }

        public PlayerStaticsActions PlayerStaticsAction
        {
            get
            {
                PlayerStaticsActions Ans = new PlayerStaticsActions();
                Ans.SetContainer(this);
                return Ans;
            }
        }

        public BatsmenActions BatsmenAction
        {
            get
            {
                BatsmenActions Ans = new BatsmenActions();
                Ans.SetContainer(this);
                return Ans;
            }
        }

        public BowlerStaticsActions BowlerStaticsAction
        {
            get
            {
                BowlerStaticsActions Ans = new BowlerStaticsActions();
                Ans.SetContainer(this);
                return Ans;
            }
        }

        public MatchResultActions MatchResultAction
        {
            get
            {
                MatchResultActions Ans = new MatchResultActions();
                Ans.SetContainer(this);
                return Ans;
            }
        }

        public TeamManagerActions TeamManagerAction
        {
            get
            {
                TeamManagerActions Ans = new TeamManagerActions();
                Ans.SetContainer(this);
                return Ans;
            }
        }

        public VirtualPlayerActions VirtualPlayerAction
        {
            get
            {
                VirtualPlayerActions Ans = new VirtualPlayerActions();
                Ans.SetContainer(this);
                return Ans;
            }
        }

        public LeagueTeamActions LeagueTeamAction
        {
            get
            {
                LeagueTeamActions Ans = new LeagueTeamActions();
                Ans.SetContainer(this);
                return Ans;
            }
        }

        public LeagueActions LeagueAction
        {
            get
            {
                LeagueActions Ans = new LeagueActions();
                Ans.SetContainer(this);
                return Ans;
            }
        }

    }
}