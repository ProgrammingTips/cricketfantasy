﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CricketFantasy.DataLayer.Modules
{
    public interface IBasicOperations<Entity> where Entity : new()
    {
        int Add(Entity t, Guid by);
        Entity Get(int id);
        Entity GetAsp(string id);
        IEnumerable<Entity> GetAll();
        void Delete(int id);
        void Update(Entity t, Guid by);
    }
}