//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CricketFantasy.DataLayer.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class LeagueInvite
    {
        public int Id { get; set; }
        public int LeagueId { get; set; }
        public string TeamManagerEmail { get; set; }
        public bool IsInviteSent { get; set; }
    
        public virtual League League { get; set; }
    }
}
